# Passit Front

passit-frontend is the angular client for [Passit](passit.io).

We use smart and dumb components with ngrx/store.
Read about it [here](https://gist.github.com/btroncone/a6e4347326749f938510#utilizing-container-components)

API and crypto interaction are handled by passit-sdk-js which itself uses libsodium.js.

Please consider supporting developoment on [gratipay](https://gratipay.com/passit/).


## Install Extensions

- [Chrome Web Store](https://chrome.google.com/webstore/detail/passit/pgcleadjbkbghamecomebcdakdmahkeh)
- [Firefox addon](https://gitlab.com/passit/passit-frontend/builds/artifacts/v0.2.4/raw/web-ext-artifacts/passit-0.2.4-an+fx.xpi?job=sign-ext)

## Development server

This project uses docker compose.

1. Install docker and docker-compose
2. Run `docker-compose up app`

The front end is now running on localhost:4200 and the backend docker image is running on localhost:8000

Live reload should be enabled, if it isn't working try increasing the max_user_watches by running

```
sudo echo "fs.inotify.max_user_watches=524288" >> /etc/sysctl.conf
sudo sysctl fs.inotify.max_user_watches=524288
```

Run any one off commands like `docker-compose run --rm app lint` to execute them inside the docker container as yarn commands.

You may want to run the client without docker, just use angular cli.

### Rebuilding when changing package.json

If you need to run `yarn install` you'll want to instead run `docker-compose build` and `docker-compose rm -v app` to remove the old container.
This is because node_modules are kept in a docker volume.
This is not ideal - if you know how to build node_modules into the container itself instead of a volume please let us know.

## Running unit tests

Run `yarn test` to execute the unit tests via [Karma](https://karma-runner.github.io).

Unit tests run in PhantomJS without a browser. This works because Crypto functions should always be mocked.
Crypto related tests should always be part of passit-sdk-js

## Running end-to-end tests

End-to-end tests are run via [Protractor](http://www.protractortest.org/).

We'll run the backend in docker and the frontend without for simplicity. Ensure you run `yarn` first to install local node modules.

1. Start backend `docker-compose up api`
2. Start dev server `yarn start`
3. Run test `yarn run e2e`

The e2e script will use the existing dev server (so you don't have to wait for it to rebuild each time)

In CI we run tests in docker. To do that run `docker-compose up -d app selenium; docker-compose run app e2e:docker`


## To Lint

```
$ yarn lint
```

## Development for web extension

The web extension works in Firefox and Chrome. It reuses the same angular code but we need to build it differently.

For development:

We'll run this time without docker (though you could use docker). We want to build the app with aot and watch enabled for auto reload.
Then we'll run web-ext to launch the code as an extension in Firefox.

1. Run backend either in docker or hosted service
2. Ensure yarn, node 6+, and [web-ext ](https://www.npmjs.com/package/web-ext) are installed
3. `yarn install` (if not done already)
4. `yarn run build:ext -- --watch`
5. (new term) `web-ext run -s dist/ -a dist/`

This process can be a little slow. You are encouraged to use test driven development or working on the web front end when possible.
Because the app shares the same code, going to routes like `/popup` actually work fine in the web version. However note when not
running as an extension you won't have access to web extension api's like the browser object.

If you'd like to try the extension in Chrome:

1. Do steps 1-4 above
2. Open up Chrome and go to Extensions
3. Check the "Developer Mode" box up top if you haven't already done so.
4. Click "Load unpacked extension..." and select the `dist/` folder.

### Deploying new extension

We don't distribute the firefox extension on the store, because they wont accept angular2 code, because it uses eval.
So instead we self host, signing it with the `web-ext` command in CI and creating an update manifest [Firefox knows when to update](https://developer.mozilla.org/en-US/Add-ons/Updates).g

1. Change version in `src/manifest.json`
3. Commit those changes
4. Add a git tag in the format `vX.X.X`
5. Push the changes and git tag to Gitlab
6. Once all build steps finish, download the `passit.zip` to upload to the chrome webstore.
   It is available as an artifact under the `build-ext-assets` job in the `test` stage. 
7. Create a new release on the [Chrome web store](https://chrome.google.com/webstore/developer/dashboard)

# Mobile app

We use NativeScript for the modile app. At this time it's not ready to use.

### Technologies used

* [Angular2](angular.io) with [TypeScript](http://www.typescriptlang.org/)
* [NativeScript](nativescript.org) for mobile
* [Webpack](https://webpack.github.io/)
* Testing [Jasmine](http://jasmine.github.io/), [Karma](https://karma-runner.github.io/1.0/index.html), and [Protractor](http://www.protractortest.org/#/)
