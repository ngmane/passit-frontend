import "rxjs/add/operator/let";
import "rxjs/add/operator/switchMap";

import { ActionReducer, ActionReducerMap } from "@ngrx/store";

import { localStorageSync } from "ngrx-store-localstorage";
import { createSelector } from "reselect";
import { Observable } from "rxjs/Observable";
import { combineLatest } from "rxjs/observable/combineLatest";

import { compose } from "@ngrx/store";
import { combineReducers } from "@ngrx/store";

import { routerReducer, RouterReducerState } from "@ngrx/router-store";
import * as popupState from "./extension/popup/popup.reducer";
import * as confState from "./get-conf/conf.reducer";
import * as contactsState from "./group/contacts/contacts.reducer";
import * as groupState from "./group/group.reducer";
import * as importerState from "./importer/importer.reducer";
import * as listState from "./list/list.reducer";
import * as secretState from "./secrets/secrets.reducer";
import { LOG_OUT } from "./shared/app-data/authState.actions";
import * as authState from "./shared/app-data/authState.reducer";

export interface IState {
    authState: authState.IAuthState;
    contacts: contactsState.IContactsState;
    group: groupState.IGroupState;
    importer: importerState.IImporterState;
    list: listState.IListState;
    router: RouterReducerState;
    secrets: secretState.ISecretState;
    popup: popupState.IPopupState;
    conf: confState.IConfState;
}

export const reducers: ActionReducerMap<IState> = {
    authState: authState.authStateReducer,
    contacts: contactsState.contactsReducer,
    group: groupState.groupReducer,
    importer: importerState.importerReducer,
    list: listState.ListReducer,
    router: routerReducer,
    secrets: secretState.secretReducer,
    popup: popupState.popupReducer,
    conf: confState.reducer,
};

// Broken at this time
// At some point we want to add localstorage sync and freeze but they don't work with ngrx 4.0
let sync = localStorageSync({
  keys: [
    "authState",
    "contacts",
    "group",
    "list",
    "secrets",
    {popup: {deserialize: (popup: popupState.IPopupState) => {
      // Only rehydrate if popup was recently interacted with
      let now = new Date();
      let lastOpened = new Date(popup.lastOpened!);
      const FIVE_MIN = 5 * 60 * 1000;
      return ((now.getTime() - lastOpened.getTime()) < FIVE_MIN) ? popup : popupState.initialState;
    }}},
    "conf",
  ],
  rehydrate: true,
});

export  const productionReducer = compose(sync, combineReducers)(reducers);
const isDev = true;

export function reducer(state: any, action: any) {
  if (action.type === LOG_OUT) {
    // Remove all state on log out
    state = undefined;
  }

  if (isDev) {
    // return developmentReducer(state, action);
    return productionReducer(state, action);
  } else {
    return productionReducer(state, action);
  }
}

export const getAuthState = (state: IState) => state.authState;
export const getisLoggedIn = createSelector(getAuthState, authState.getIsLoggedIn);
export const getIsInited = createSelector(getAuthState, authState.getIsInited);
export const getEmail = createSelector(getAuthState, authState.getEmail);
export const getToken = createSelector(getAuthState, authState.getToken);
export const getUrl = createSelector(getAuthState, authState.getUrl);
export const getRedirectUrl = createSelector(getAuthState, authState.getRedirectUrl);

export const getImporterState = (state: IState) => state.importer;
export const getImporterSecrets = createSelector(getImporterState, importerState.getSecrets);
export const getImporterFileName = createSelector(getImporterState, importerState.getFileName);

export const getSecretState = (state: IState) => state.secrets;
export const getSecrets = createSelector(getSecretState, secretState.getSecrets);

export const getListState = (state: IState) => state.list;
export const getListShowCreate = createSelector(getListState, listState.getListShowCreate);

export const getGroupState = (state: IState) => state.group;
export const getGroups = createSelector(getGroupState, groupState.getGroups);
export const getGroupShowCreate = createSelector(getGroupState, groupState.getGroupShowCreate);
export const getGroupForm = createSelector(getGroupState, groupState.getGroupForm);
export const getGroupManaged = createSelector(getGroupState, groupState.getGroupManaged);

export const getContactsState = (state: IState) => state.contacts;
export const getContacts = createSelector(getContactsState, contactsState.getContacts);

export const getPopupState = (state: IState) => state.popup;
export const getPopupSelected = createSelector(getPopupState, popupState.getSelectedSecret);
export const getPopupSearch = createSelector(getPopupState, popupState.getSearch);

export const getConfState = (state: IState) => state.conf;
export const getIsDemo = createSelector(getConfState, confState.getIsDemo);
export const getTimestamp = createSelector(getConfState, confState.getTimestamp);
