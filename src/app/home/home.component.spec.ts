import { DebugElement } from "@angular/core";
import { ComponentFixture, ComponentFixtureAutoDetect, TestBed } from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";

import { By } from "@angular/platform-browser";
import { StoreModule } from "@ngrx/store";
import { InlineSVGModule } from "ng-inline-svg";
import { reducers } from "../app.reducers";

import { HomeComponent } from "./home.component";

let comp: HomeComponent;
let fixture: ComponentFixture<HomeComponent>;
let de: DebugElement;
let el: HTMLElement;

let mockStore = {
  auth: {
    isLoggedIn: true,
  }
};

describe("Home Component: ", () => {
  // provide our implementations or mocks to the dependency injector
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeComponent ],
      imports: [
        InlineSVGModule,
        RouterTestingModule.withRoutes([
        ]),
        StoreModule.forRoot(reducers, {initialState: () => mockStore}),
      ],
      providers: [
        { provide: ComponentFixtureAutoDetect, useValue: true }
      ],
    });

    // create component and test fixture
    fixture = TestBed.createComponent(HomeComponent);

    // HomeComponent test instance
    comp = fixture.componentInstance;

    // query for title by css element selector
    de = fixture.debugElement.query(By.css("h2"));
    el = de.nativeElement;
  });

  it("should display 'Passit' title", () => {
    expect(el.textContent).toBe("Welcome to Passit");
  });

});
