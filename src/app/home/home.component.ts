import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { Store } from "@ngrx/store";
import { Subscription } from "rxjs";
import { getAuthState, IState } from "../app.reducers";

@Component({
  selector: "home",
  styleUrls: ["./home.component.scss"],
  templateUrl: "./home.component.html"
})

export class HomeComponent {
  private storeSubscription: Subscription;

  constructor(private router: Router, private store: Store<IState>) {
  }

  ngOnInit() {
    this.storeSubscription = this.store.select(getAuthState).subscribe((auth) => {
      if (auth.isLoggedIn) {
        this.router.navigate(["/list"]);
      } else {
        this.router.navigate(["/login"]);
      }
    });
  }

  ngOnDestroy() {
    this.storeSubscription.unsubscribe();
  }
}
