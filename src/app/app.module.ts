/*
* @angular
*/
import { APP_BASE_HREF } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { BrowserModule  } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { environment } from "../environments/environment";
import { routing, routingStore } from "./app.routing";

/*
* Third Party Other
*/
import { StoreModule } from "@ngrx/store";
import { StoreDevtoolsModule } from "@ngrx/store-devtools";
import { InlineSVGModule } from "ng-inline-svg";
import { SelectModule } from "ng-select";
import { ClipboardModule } from "ngx-clipboard";
import { TooltipModule } from "ngx-tooltip";

/*
* Passit
*/
import { AccountModule } from "./account";
import { AppComponent } from "./app.component";
import { ExporterComponent } from "./exporter";
import {
  GroupAddButtonComponent,
  GroupAddComponent,
  GroupContainer,
  GroupDetailComponent,
  GroupFormComponent,
  GroupListComponent,
} from "./group";
import { LoggedInGuard } from "./guards";
import { HomeComponent } from "./home";
import {
  ImporterComponent,
  ImporterContainer,
} from "./importer";
import {
  SecretAddComponent,
  SecretFormComponent,
  SecretListComponent,
  SecretListContainer,
  SecretRowComponent,
} from "./list";
import { NoContentComponent } from "./no-content";
import { SharedModule } from "./shared";

import { ExtensionModule } from "./extension";
import { ProgressIndicatorModule } from "./progress-indicator";

import { productionReducer, reducer, reducers } from "./app.reducers";
import { ExporterService } from "./exporter/exporter.service";
import { GetConfService } from "./get-conf/";
import { GroupService } from "./group";
import { ContactsService } from "./group/contacts/contacts.service";
import { ImporterService } from "./importer/importer.service";
import { Api } from "./ngsdk/api";
import { NgPassitSDK } from "./ngsdk/sdk";
import { SecretService } from "./secrets/secret.service";
import { AppDataService } from "./shared/app-data/app-data.service";

/* tslint:disable:object-literal-sort-keys */
@NgModule({
  declarations: [
    AppComponent,
    SecretAddComponent,
    GroupContainer,
    GroupAddComponent,
    GroupListComponent,
    GroupAddButtonComponent,
    GroupDetailComponent,
    GroupFormComponent,
    HomeComponent,
    SecretListComponent,
    SecretListContainer,
    ImporterContainer,
    ImporterComponent,
    NoContentComponent,
    SecretFormComponent,
    SecretRowComponent,
    ExporterComponent,
  ],
  imports: [
    AccountModule,
    BrowserModule,
    BrowserAnimationsModule,
    ClipboardModule,
    FormsModule,
    HttpModule,
    InlineSVGModule,
    ProgressIndicatorModule,
    ReactiveFormsModule,
    routing,
    routingStore,
    SelectModule,
    StoreModule.forRoot(reducers, {metaReducers: []}),
    !environment.production ? StoreDevtoolsModule.instrument({maxAge: 25}) : [],
    SharedModule,
    TooltipModule,
    ExtensionModule,
  ],
  providers: [
    {
      provide: APP_BASE_HREF,
      useValue: "/"
    },
    GetConfService,
    { provide: Api, useClass: Api },
    { provide: AppDataService, useClass:  AppDataService},
    { provide: GroupService, useClass: GroupService},
    { provide: ContactsService, useClass: ContactsService},
    { provide: ImporterService, useClass: ImporterService},
    LoggedInGuard,
    { provide: ExporterService, useClass: ExporterService},
    { provide: NgPassitSDK, useClass: NgPassitSDK},
    { provide: SecretService, useClass: SecretService},
  ],
  bootstrap: [ AppComponent ]
})

export class AppModule { }
