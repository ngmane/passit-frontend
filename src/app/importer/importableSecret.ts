import { INewSecret } from "passit-sdk-js/passit_sdk/interfaces";

export interface ImportableSecret extends INewSecret {
  doImport: boolean;
  importable: boolean;
}
