/*
* @angular
*/
import { Component, ViewEncapsulation } from "@angular/core";
import { Title } from "@angular/platform-browser";
import { Router, RoutesRecognized } from "@angular/router";

/*
* passit
*/
import { AppDataService } from "./shared/app-data/app-data.service";

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: "app-root",
  styleUrls: ["./app.style.scss"],
  template: `
    <navbar-container></navbar-container>
    <main>
      <router-outlet></router-outlet>
    </main>
  `
})

export class AppComponent {
  public title: string;

  constructor(
    private appDataService: AppDataService,
    private router: Router,
    private titleService: Title) {
    this.appDataService.rehydrate();
    this.title = "Passit";

    router.events.subscribe((event) => {
      if (event instanceof RoutesRecognized) {
        let titleTag = "Passit";
        if (typeof event.state.root.firstChild!.data["title"] !== "undefined") {
          titleTag = event.state.root.firstChild!.data["title"] + " | Passit";
        }
        titleService.setTitle(titleTag);
      }
    });
  }
}
