import { ActionReducer } from "@ngrx/store";
import * as authState from "./authState.actions";

export interface IAuthState {
    email: string | null;
    url: string | null;
    isInited: boolean;
    isLoggedIn: boolean;
    redirectUrl: string | null;
    userToken: string | null;
}

const initialState: IAuthState = {
  email: null,
  url: null,
  isInited: false,
  isLoggedIn: false,
  redirectUrl: null,
  userToken: null,
};

export function authStateReducer(state = initialState, action: authState.Actions): IAuthState {
  switch (action.type) {
    case authState.LOG_IN:
      return Object.assign({}, state, {
        email: action.payload.email,
        userToken: action.payload.token,
        isLoggedIn: true,
      });

    case authState.LOG_OUT:
      localStorage.removeItem("auth");
      return Object.assign({}, state, {
        isInited: true,  // Log out wipes state, except keeps isInited true
      });

    case authState.SET_URL:
      return Object.assign({}, state, {
        url: action.payload,
      });

    case authState.SET_REDIRECT_URL:
      return Object.assign({}, state, {
        redirectUrl: action.payload,
      });

    case authState.CLEAR_REDIRECT_URL:
      return Object.assign({}, state, {
        redirectUrl: null,
      });

    case authState.INIT:
      return Object.assign({}, state, {
        isInited: true,
      });

    default:
      return state;
  }
}

export const getIsLoggedIn = (state: IAuthState) => state.isLoggedIn;
export const getIsInited = (state: IAuthState) => state.isInited;
export const getToken = (state: IAuthState) => state.userToken;
export const getEmail = (state: IAuthState) => state.email;
export const getUrl = (state: IAuthState) => state.url;
export const getRedirectUrl = (state: IAuthState) => state.redirectUrl;
