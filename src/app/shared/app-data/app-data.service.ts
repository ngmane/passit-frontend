import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";

import { UserService } from "../../account/user";
import * as fromRoot from "../../app.reducers";
import { IState } from "../../app.reducers";
import { GetConfService } from "../../get-conf/";
import { INIT } from "../app-data/authState.actions";

@Injectable()
export class AppDataService {
  constructor(
    public userService: UserService,
    private getConfService: GetConfService,
    private store: Store<IState>,
  ) {}

  public rehydrate() {
    let promises: Array<Promise<any>> = [];
    promises.push(this.userService.rehydrate());
    promises.push(this.getConfService.rehydrate());

    Promise.all(promises).then(() => {
      this.store.dispatch({ type: INIT });
    });
  }
}
