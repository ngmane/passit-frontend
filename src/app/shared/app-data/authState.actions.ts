/* tslint:disable:max-classes-per-file */
import { Action } from "@ngrx/store";

export const LOG_IN = "LOG_IN";
export const LOG_OUT = "LOG_OUT";
export const SET_URL = "SET_URL";
export const CLEAR_REDIRECT_URL = "CLEAR_REDIRECT_URL";
export const SET_REDIRECT_URL = "SET_REDIRECT_URL";
export const INIT = "INIT";

export class LogInAction implements Action {
  readonly type = LOG_IN;

  constructor(public payload: any) { }
}

export class LogOutAction implements Action {
  readonly type = LOG_OUT;
}

export class SetUrlAction implements Action {
  readonly type = SET_URL;

  constructor(public payload: string) { }
}

export class ClearRedirectUrlAction implements Action {
  readonly type = CLEAR_REDIRECT_URL;
}
export class SetRedirectUrlAction implements Action {
  readonly type = SET_REDIRECT_URL;

  constructor(public payload: string) { }
}
export class InitAction implements Action {
  readonly type = INIT;
}

export type Actions
  = LogInAction
  | LogOutAction
  | SetUrlAction
  | ClearRedirectUrlAction
  | SetRedirectUrlAction
  | InitAction;
