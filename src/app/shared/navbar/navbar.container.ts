import { Component, Input } from "@angular/core";
import { Router, RoutesRecognized } from "@angular/router";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs/Observable";
import { getAuthState, getIsDemo, IState } from "../../app.reducers";
import { LOG_OUT } from "../app-data/authState.actions";

@Component({
  selector: "navbar-container",
  template: `
    <demo *ngIf="isDemo$ | async"></demo>
    <navbar
      [isLoggedIn]="isLoggedIn"
      (logout)="logout()"
      *ngIf="isVisible"
    ></navbar>
  `
})

export class NavbarContainer {
  isLoggedIn: boolean;
  isDemo$: Observable<boolean>;
  isVisible: boolean;

  constructor(private router: Router, private store: Store<IState>) {
    this.store.select(getAuthState).subscribe((auth) => {
      this.isLoggedIn = auth.isLoggedIn;
    });
    this.isDemo$ = this.store.select(getIsDemo);
    this.isVisible = true;

    router.events.subscribe((event) => {
      if (event instanceof RoutesRecognized) {
        if (typeof event.state.root.firstChild!.data["showNavBar"] !== "undefined" ) {
          this.isVisible = event.state.root.firstChild!.data["showNavBar"];
        } else {
          this.isVisible = true;
        }
      }
    });
  }

  /*
  * remove user from local storage to log user out
  * navigate to home
  */
  logout() {
    this.store.dispatch({ type: LOG_OUT });
    this.router.navigate(["/login"]);
  }
}
