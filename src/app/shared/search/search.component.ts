import { Component, EventEmitter, Input } from "@angular/core";

@Component({
  outputs: ["searchTerm", "reset"],
  selector: "search",
  styleUrls: ["./search.component.scss"],
  templateUrl: "./search.component.html"
})

export class SearchComponent {
  @Input() query: string = "";
  public reset: EventEmitter<boolean>;
  public searchTerm: EventEmitter<string>;

  constructor() {
    // this.query = "";
    this.reset = new EventEmitter();
    this.searchTerm = new EventEmitter();
  }

  /*
  * when reset button is clicked
  * reset this.query value
  * emit truth to parent (list)
  */
  public resetSearch(): void {
    this.query = "";
    this.reset.emit(true);
  }

  /*
  * when enter key pressed or button clicked
  * assign value of input to this.query
  * take value of input and emit it to parent (list)
  */
  public submitSearch(term: string) {
    this.query = term;
    this.searchTerm.emit(term);
  }

}
