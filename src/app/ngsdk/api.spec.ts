import { fakeAsync, inject, TestBed } from "@angular/core/testing";
import {HttpModule, Response, ResponseOptions, XHRBackend} from "@angular/http";
import {MockBackend, MockConnection} from "@angular/http/testing";
import { StoreModule } from "@ngrx/store";
import { reducers } from "../app.reducers";
import { Api } from "./api";

let mockStore = {
  auth: {
    isLoggedIn: true,
  }
};

describe("Testing sdk api: ", () => {

  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpModule,
      StoreModule.forRoot(reducers, {initialState: () => mockStore}),
    ],
    providers: [
      {
        provide: XHRBackend,
        useClass: MockBackend
      },
      Api,
    ]
  }));

  it("should be able to make GET requests",
  inject([XHRBackend, Api], fakeAsync((mockBackend: MockBackend, api: Api) => {
    mockBackend.connections.subscribe(
      (connection: MockConnection) => {
        connection.mockRespond(new Response(
          new ResponseOptions({body: ["something"]}
        )));
      }
    );

    api.jsonGet("test-url").then((resp) => {
      expect(resp[0]).toBe("something");
    });
  })));
});
