import { Injectable } from "@angular/core";
import PassitSDK from "passit-sdk-js";
import { Api } from "./api";

/**
 * Angular wrapper of PassitSDK that swaps out the api class with one that uses
 * $http instead of fetch. This is done for better integration with angular
 * tooling like Protractor.
 */
@Injectable()
export class NgPassitSDK extends PassitSDK {
    protected api: any;  // Is there a better way than any?

    constructor(api: Api) {
        super();
        this.api = api;
    }

    public set_up(publicKey: string, privateKey: string, userId: number) {
        this.userId = userId;
        return this.set_keys(publicKey, privateKey);
    }

    public setUrl(url: string) {
        this.api.setUrl(url);
    }

    public confirm_email(code: string): Promise<any> {
      return this.api.jsonPut("confirm-email/", {code});
    }

}
