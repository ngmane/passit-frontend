import { Component, EventEmitter, Output } from "@angular/core";

@Component({
  selector: "app-delete",
  templateUrl: "./delete.component.html",
  styleUrls: ["./delete.component.scss"]
})
export class DeleteComponent {
  canDelete = false;

  @Output() doAccountDelete = new EventEmitter();

  updateValue = (value: string) => {
    this.canDelete = (value === "DELETE") ? true : false;
  }
}
