import { Component } from "@angular/core";
import { UserService } from "../user/user.service";

@Component({
  selector: "app-delete-container",
  template: `
    <app-delete
      (doAccountDelete)="doAccountDelete()"
    ></app-delete>
  `
})
export class DeleteContainer {
  constructor(private userService: UserService) { }

  doAccountDelete() {
    this.userService.deleteUserAccount();
  }
}
