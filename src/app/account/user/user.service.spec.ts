import { fakeAsync, inject, TestBed } from "@angular/core/testing";
import {
  BaseRequestOptions,
  Http,
  HttpModule,
  Response,
  ResponseOptions,
  ResponseOptionsArgs,
  XHRBackend,
} from "@angular/http";
import { MockBackend, MockConnection } from "@angular/http/testing";
import { StoreModule } from "@ngrx/store";

import { User } from "./user";
import { UserService } from "./user.service";

import { Store } from "@ngrx/store";
import { getUrl, IState, reducers } from "../../app.reducers";
import { NgPassitSDK } from "../../ngsdk/sdk";

let sdkStub = {
  is_username_available: () => {
    return Promise.resolve(true);
  },
};
let storeStub = {};

function mockIt(mockBackend: MockBackend, mockData: ResponseOptionsArgs) {
  mockBackend.connections.subscribe(
    (connection: MockConnection) => {
      connection.mockRespond(
        new Response(
          new ResponseOptions(mockData)
        )
      );
    }
  );
}

describe("Service: UserService", () => {
  let service: UserService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpModule,
        StoreModule.forRoot(reducers),
      ],
      providers: [
        {provide: NgPassitSDK, useValue: sdkStub },
        {provide: XHRBackend, useClass: MockBackend},
        UserService,
      ]
    });
  });

  beforeEach(inject([UserService, ], (userService: UserService) => {
    service = userService;
  }));

  it("Can set up SDK", (done) => {
    let user: User = {
      email: "test@example.com",
      password: "mypass",
    };

    service.checkUsername(user.email).then((isAvail) => {
      expect(isAvail.isAvailable).toBe(true);
      done();
    });
  });

  it("Can check and set url",
    fakeAsync(inject([XHRBackend, UserService, Store],
    (mockBackend: any, userService: any, store: Store<IState>
  ) => {
    let correctUrl = "https://api.passit.io/api/";
    mockIt(mockBackend, {
      body: "{}",
      url: correctUrl,
    });
    let url = "passit.io"; // The service should prefix https and api
    userService.checkAndSetUrl(url).then(() => {
      store.select(getUrl).subscribe((storeUrl) => {
        expect(storeUrl).toBe(correctUrl);
      });
    });
  })));
});
