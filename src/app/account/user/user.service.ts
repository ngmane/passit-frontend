import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Store } from "@ngrx/store";
import "rxjs/add/operator/toPromise";

import { getAuthState, IState } from "../../app.reducers";
import * as fromRoot from "../../app.reducers";
import { IPassitSDKError } from "../../ngsdk";
import { NgPassitSDK } from "../../ngsdk/sdk";
import { LOG_IN, SET_URL } from "../../shared/app-data/authState.actions";
import { IAuthStore } from "./user.interfaces";

@Injectable()
export class UserService {
  constructor(private sdk: NgPassitSDK, private store: Store<IState>, private http: Http) {}

  /*
  * Check if username is available
  * returns true or false
  * if true: RegisterComponent calls register method below
  * if false: alert message triggered in RegisterComponent
  */
  public checkUsername(email: string): Promise<{isAvailable: boolean, error?: IPassitSDKError}> {
    return this.sdk.is_username_available(email)
      .then((isAvailable) => {
        return {
          isAvailable,
        };
      })
      .catch((err: IPassitSDKError) => {
        return {
          isAvailable: false,
          error: err,
        };
      });
  }

  /* Figure out the URL before doing anything else */
  public checkAndSetUrl(url: string) {
    return new Promise((resolve, reject) => {
      // strip https
      url = url.replace(/^https?\:\/\//i, "");

      // normalize ending with /api/
      url = url.replace(/\/api\/?/i, "");
      url = url + "/api/";

      // Try api.url first
      let apiUrl = "https://api." + url;
      this.http.get(apiUrl + "ping/").toPromise()
        .then(() => {
          this.setSdkUrl(apiUrl);
          resolve(apiUrl);
        })
        .catch(() => {
          // Try non api url
          // Force https only
          url = "https://" + url;
          return this.http.get(url + "ping/").toPromise()
            .then(() => {
              this.setSdkUrl(url);
              resolve(url);
            }).catch(() => {
              reject("invalid");
            });
        });
    });
  }

  /*
  * send login get to sdk
  * email and password to login user
  */
  public login(email: string, password: string): Promise<any> {
    return this.sdk.log_in(
      email,
      password
    ).then((resp) => {
      let auth: IAuthStore = {
        privateKey: new Buffer(resp.privateKey).toString("base64"),
        publicKey: resp.user.public_key,
        userId: resp.user.id,
        email,
        userToken: resp.token,
      };

      localStorage.setItem("auth", JSON.stringify(auth));

      return this.setUp(auth);
    }).catch((err) => Promise.reject(err));
  }

  /** Change the users password - note this can take some time.
   */
  public changePassword(newPassword: string): Promise<any> {
    return new Promise((resolve, reject) => {
      let email$ = this.store.select(fromRoot.getEmail);
      email$.take(1).subscribe((email) => {
        this.sdk.change_password(newPassword)
        .then(() => resolve())
        .catch((error) => console.error("Error in changing password", error));
      });
    });
  }

  /** Delete User Account forever. */
  public deleteUserAccount() {
    return null;
  }

  /*
  * create user post to sdk
  * pass email and password to sdk to register user
  */
  public register(email: string, password: string) {
    return this.sdk.sign_up(email, password).then((resp) => {
      return resp;
    }).catch((err) => {
      console.error(err);
    });

  }

  /** call set_keys user service method give it public and private key */
  public rehydrate(): Promise<any> {
    if (localStorage.getItem("auth")) {
      let auth = JSON.parse(localStorage.getItem("auth") as string);
      return this.setUp(auth);
    }
    return Promise.resolve();
  }

  private setSdkUrl(url: string) {
    this.store.dispatch({
      type: SET_URL,
      payload: url,
    });
  }

  /** Set state for user auth */
  private setUp(auth: IAuthStore): Promise<any> {
    return new Promise((resolve, reject) => {
      this.sdk.set_up(auth.publicKey, auth.privateKey, auth.userId);
      this.store.dispatch({
        type: LOG_IN,
        payload: {
          email: auth.email,
          token: auth.userToken,
        }
      });
      resolve();
    });
  }
}
