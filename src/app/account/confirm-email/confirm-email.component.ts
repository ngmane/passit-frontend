import { Component, EventEmitter, Input, Output } from "@angular/core";
import { FormsModule } from "@angular/forms";

import { IS_EXTENSION } from "../../constants";
import { User } from "../user";

/**
 * Confirm email component
 */
@Component({
  selector: "confirm-email-component",
  templateUrl: "./confirm-email.component.html"
})
export class ConfirmEmailComponent {
  /**
   * The error message, passed from the container to the template
   */
  @Input() errorMessage: string;

  /**
   * The confirmation code. Can be pulled as a param from the URL or entered in the text field.
   */
  @Input() code: string;

  /**
   * Lets the template know if a server call has started
   */
  @Input() hasStarted: boolean;

  /**
   * Lets the template know if a server call has finished (successfully)
   */
  @Input() hasFinished: boolean;

  /**
   * Emits the confirmation code to the container and triggers the server call
   */
  @Output() confirmEmail = new EventEmitter<string>();

  /**
   * The form uses this to trigger the confirmEmail emitter
   *
   * @param {code: string} form The code used to confirm the email address
   */
  onSubmit(form: {code: string}) {
    this.confirmEmail.emit(form.code);
  }
}
