import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Store } from "@ngrx/store";

import { NgPassitSDK } from "../../ngsdk/sdk";

import * as fromRoot from "../../app.reducers";
import { CLEAR_REDIRECT_URL } from "../../shared/app-data/authState.actions";

/**
 * Confirm email container
 */
@Component({
  template: `
    <confirm-email-component
      [hasStarted]="hasStarted"
      [hasFinished]="hasFinished"
      [errorMessage]="errorMessage"
      [code]="code"
      (confirmEmail)="confirmEmail($event)"
    ></confirm-email-component>
  `,
})
export class ConfirmEmailContainer {
  /**
   * The error message, passed to the template
   */
  errorMessage: string;

  /**
   * Lets the template know if a server call has started
   */
  hasStarted = false;

  /**
   * Lets the template know if a server call has finished (successfully)
   */
  hasFinished = false;

  /**
   * Lets the page redirect if necessary
   */
  redirectUrl: string | null = null;

  /**
   * The confirmation code
   */
  code = "";

  /**
   * Constructor
   *
   * @param {Router} router A Router
   * @param {ActivatedRoute} route An ActivatedRoute
   * @param {Store} store The ngrx store
   * @param {NgPassitSDK} ngPassitSDK An instance of the Passit SDK
   */
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<fromRoot.IState>,
    private ngPassitSDK: NgPassitSDK) {
    this.store.select(fromRoot.getRedirectUrl).subscribe((redirectUrl) => {
      this.redirectUrl = redirectUrl;
    });
  }

  /**
   * On init, look for the code param in the URL. If it's there, submit it.
   */
  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.code = params["code"];
      if (params["code"]) {
        this.confirmEmail(params["code"]);
      }
    });
  }

  /**
   * Submit function
   * If code submits successfully, redirect away.
   * Otherwise, throw an error that the template can read
   *
   * @param {string} code The code used to confirm the email address
   */
  confirmEmail(code: string) {
    if (typeof code === "undefined" || code === "") {
        return this.errorMessage = "Please enter your code here.";
    } else {
      this.hasStarted = true;
      this.ngPassitSDK.confirm_email(code)
        .then((resp) => {
          this.hasStarted = false;
          this.hasFinished = true;
          if (this.redirectUrl) {
            this.router.navigate([this.redirectUrl]);
            this.store.dispatch({type: CLEAR_REDIRECT_URL});
          } else {
            this.router.navigate(["/list"]);
          }
        }).catch((res) => {
          this.hasStarted = false;
          this.hasFinished = false;
          if (res.status === 404) {
            return this.errorMessage = "Please enter a valid code.";
          }
          this.errorMessage = "Unexpected error.";
        });
    }
  }
}
