import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from "@angular/forms";
import { flyInOut } from "../../app.animations";
import { ProgressIndicatorComponent } from "../../progress-indicator";

@Component({
  selector: "change-password",
  styleUrls: ["./change-password.styles.scss"],
  templateUrl: "./change-password.component.html",
  animations: [flyInOut],
})
export class ChangePasswordComponent implements OnInit {
  @Input() hasStarted: boolean;
  @Input() hasFinished: boolean;
  @Output() changePassword = new EventEmitter<string>();

  changePasswordForm: FormGroup;
  formErrors = {
    password: [],
    passwordConfirm: []
  };
  showConfirm = true;
  submitted = false;
  validationMessages = {
    password: {
      required: "You must have a password!",
      minlength: "Password must be at least 10 characters long.",
    },
    passwordConfirm: {
      required: "Confirm your password by entering it again here, or click the\
        eye icon to visually inspect your new password.",
      passwordMatch: "Passwords don't match.",
    }
  };

  constructor(private fb: FormBuilder) { }

  toggleConfirm() {
    this.showConfirm = !this.showConfirm;
  }

  onSubmit() {
    this.submitted = true;
    // The second conditional is cheating probably. Ideally I'm doing some kind
    // of conditional validator on passwordConfirm so it's not required (and
    // thus valid) if we're not showing it.
    if (this.changePasswordForm.valid ||
        this.changePasswordForm.controls["password"]["valid"] && !this.showConfirm) {
      this.changePassword.emit(this.changePasswordForm.value.password);
    } else {
      // console.log("form invalid");
    }
  }

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm(): void {
    this.changePasswordForm = this.fb.group({
      password: ["", [
          Validators.required,
          Validators.minLength(10),
        ]
      ],
      passwordConfirm: ["", Validators.compose(
          [
            Validators.required,
            this.matchPassword(),
          ]
        )]
    });
    this.changePasswordForm.valueChanges
      .subscribe((data) => this.onValueChanged(data));
    this.onValueChanged(); // (re)set validation messages now
  }

  matchPassword(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const password = control.value;
      const passwordOtherField = this.changePasswordForm ? this.changePasswordForm.value.password : null;
      const match = password === passwordOtherField;
      return !match ? {passwordMatch: {password}} : null;
    };
  }

  onValueChanged(data?: any) {
    if (!this.changePasswordForm) { return; }
    const form = this.changePasswordForm;

    for (const field in this.formErrors) {
      // clear previous error message (if any)
      if (this.formErrors.hasOwnProperty(field)) {
        this.formErrors[field] = [];
        const control = form.get(field);

        if (control && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors!) {
            if (control.errors!.hasOwnProperty(key)) {
              this.formErrors[field].push(messages[key]);
            }
          }
        }
      }
    }
  }
}
