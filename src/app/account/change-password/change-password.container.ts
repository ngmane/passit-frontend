import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { Store } from "@ngrx/store";

import * as fromRoot from "../../app.reducers";
import { LOG_OUT } from "../../shared/app-data/authState.actions";
import { UserService } from "../user";

@Component({
  selector: "change-password-container",
  template: `
  <change-password
    [hasStarted]="hasStarted"
    [hasFinished]="hasFinished"
    (changePassword)="changePassword($event)">
  </change-password>
  `
})
export class ChangePasswordContainer {
  hasStarted = false;
  hasFinished = false;

  constructor(
    private store: Store<fromRoot.IState>,
    private router: Router,
    private userService: UserService,
  ) { }

  changePassword(newPassword: string) {
    this.hasStarted = true;
    this.hasFinished = false;
    setTimeout(() => { // TODO Remove timeout when web workers are implemented
      this.userService.changePassword(newPassword).then(() => {
        this.hasStarted = false;
        this.hasFinished = true;
        this.store.dispatch({ type: LOG_OUT });
        this.router.navigate(["/login"]);
        setTimeout(() => this.hasFinished = false, 5000);
      });
    }, 200);
  }

}
