export interface IRegisterForm {
  email: string;
  password: string;
  url?: string;
}
