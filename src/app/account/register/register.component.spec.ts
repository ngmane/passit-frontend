/*
* @angular
*/
import { DebugElement } from "@angular/core";
import { ComponentFixture, TestBed } from "@angular/core/testing";
import { By } from "@angular/platform-browser";

/*
* Passit
*/
import { RegisterComponent } from "./register.component";

/*
* test setup
*/
let comp: any;
let fixture: ComponentFixture<RegisterComponent>;
let de: DebugElement;
let el: HTMLElement;

describe("Testing Register Component: ", () => {

  beforeEach( () => {

    TestBed.configureTestingModule({
      declarations: [ RegisterComponent ]
    });

    fixture = TestBed.createComponent(RegisterComponent);
    comp = fixture.componentInstance;

    // check for title, get by CSS selector
    de = fixture.debugElement.query(By.css("h2"));
    el = de.nativeElement;

    it("title should be present", () => {
      expect(el.textContent).toContain(comp.title);
    });

  });

});
