import { Component } from "@angular/core";
import { Router } from "@angular/router";

import { UserService } from "../user";
import { IRegisterForm } from "./interfaces";

@Component({
  providers: [ UserService ],
  template: `
    <register-component
      [hasRegisterStarted]="hasRegisterStarted"
      [hasRegisterFinished]="hasRegisterFinished"
      [errorMessage]="errorMessage"
      (register)="register($event)"
      (goToLogin)="goToLogin()"
    ></register-component>
  `,
})
export class RegisterContainer {
  errorMessage: string;
  hasRegisterStarted = false;
  hasRegisterFinished = false;

  constructor(private router: Router, private userService: UserService) {
  }

  goToLogin() {
    this.router.navigate(["/login"]);
  }

  /*
  * first call checkUsername method on UserService
  * if return true, call register method on UserService
  * then navigate to login page for new user to login
  * if it returns false the username is already taken
  * currently set to alert user to try again
  */
  register(event: IRegisterForm) {
    this.hasRegisterStarted = true;
    let promises: any = [];
    if (event.url) {
      promises.push(this.userService.checkAndSetUrl(event.url));
    }
    setTimeout(() => {
      Promise.all(promises).then(() => {
        this.userService.checkUsername(event.email)
          .then((resp) => {
            let isAvailable = resp.isAvailable;
            if (isAvailable === true) {
              this.userService.register(event.email, event.password)
                .then(() => {
                  this.hasRegisterStarted = false;
                  this.hasRegisterFinished = true;
                  this.router.navigate(["/confirm-email"]);
                });
            } else if (isAvailable === false) {
              this.hasRegisterStarted = false;
              if (resp.error) {
                let x = resp.error.res.json();
                if (x.email) {
                  this.errorMessage = x.email[0];
                } else {
                  this.errorMessage = "Unknown error";
                }
              } else {
                this.errorMessage = "There is already an account associated with this email address.";
              }
            }
          });
      });
    }, 200);
  }
}
