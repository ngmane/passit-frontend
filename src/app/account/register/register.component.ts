import { Component, EventEmitter, Input, Output } from "@angular/core";

import { flyInOut } from "../../app.animations";
import { IS_EXTENSION } from "../../constants";
import { ProgressIndicatorComponent } from "../../progress-indicator";
import { User } from "../user/user";
import { IRegisterForm } from "./interfaces";

@Component({
  selector: "register-component",
  styleUrls: ["./auth.components.scss"],
  templateUrl: "./register.component.html",
  animations: [flyInOut],
})

export class RegisterComponent {
  @Input() errorMessage: string;
  @Input() hasRegisterStarted: boolean;
  @Input() hasRegisterFinished: boolean;
  @Output() register = new EventEmitter<IRegisterForm>();
  @Output() goToLogin = new EventEmitter();
  passwordFocused = false;
  user: User;
  showServer = IS_EXTENSION;

  constructor() {
    this.user = new User();
  }

  focusPasswordToggle = (focus: boolean) => {
    this.passwordFocused = focus ? true : false;
  }

  onSubmit(form: IRegisterForm) {
    this.focusPasswordToggle(false);
    this.register.emit({email: form.email, password: form.password, url: form.url});
  }

  toggleDisplay() {
    this.goToLogin.emit();
  }
}
