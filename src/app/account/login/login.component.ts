import { Component, EventEmitter, Input, Output } from "@angular/core";
import { FormsModule } from "@angular/forms";

import { IS_EXTENSION } from "../../constants";
import { User } from "../user";
import { ILoginForm } from "./interfaces";

@Component({
  selector: "login-component",
  styleUrls: ["../register/auth.components.scss"],
  templateUrl: "./login.component.html"
})
export class LoginComponent {
  @Input() errorMessage: string;
  @Input() hasLoginStarted: boolean;
  @Input() hasLoginFinished: boolean;
  @Output() login = new EventEmitter<ILoginForm>();
  @Output() goToRegister = new EventEmitter();
  showServer = IS_EXTENSION;

  user: User;

  constructor() {
    this.user = new User();
  }

  onSubmit(form: ILoginForm) {
    this.login.emit({email: form.email, password: form.password, url: form.url});
  }

  toggleDisplay() {
    this.goToRegister.emit();
  }
}
