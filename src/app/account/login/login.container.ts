import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { Store } from "@ngrx/store";

import { UserService } from "../user";
import { ILoginForm } from "./interfaces";

import * as fromRoot from "../../app.reducers";
import { CLEAR_REDIRECT_URL } from "../../shared/app-data/authState.actions";

@Component({
  template: `
    <login-component
      [hasLoginStarted]="hasLoginStarted"
      [hasLoginFinished]="hasLoginFinished"
      [errorMessage]="errorMessage"
      (login)="login($event)"
      (goToRegister)="goToRegister()"
    ></login-component>
  `,
})
export class LoginContainer {
  errorMessage: string;
  hasLoginStarted = false;
  hasLoginFinished = false;
  redirectUrl: string | null = null;
  constructor(
    private router: Router,
    private store: Store<fromRoot.IState>,
    private userService: UserService) {
    this.store.select(fromRoot.getRedirectUrl).subscribe((redirectUrl) => {
      this.redirectUrl = redirectUrl;
    });

  }

  goToRegister() {
    this.router.navigate(["/register"]);
  }

  login(event: ILoginForm) {
    this.hasLoginStarted = true;
    let promises = [];
    if (event.url) {
      promises.push(this.userService.checkAndSetUrl(event.url));
    }
    Promise.all(promises).then(() => {
      this.userService.login(event.email, event.password)
      .then((resp) => {
        this.hasLoginStarted = false;
        this.hasLoginFinished = true;
        if (this.redirectUrl) {
          this.router.navigate([this.redirectUrl]);
          this.store.dispatch({type: CLEAR_REDIRECT_URL});
        } else {
          this.router.navigate(["/list"]);
        }
      }).catch((err) => {
        this.hasLoginStarted = false;
        this.hasLoginFinished = false;
        if (err.res && (err.res.status === 401 || err.res.status === 404)) {
          return this.errorMessage = "Incorrect username or password.";
        }
        this.errorMessage = "Unexpected error.";
      });
    }).catch((errs) => {
      this.hasLoginStarted = false;
      this.hasLoginFinished = false;
      this.errorMessage = "Invalid Server URL";
    });
  }
}
