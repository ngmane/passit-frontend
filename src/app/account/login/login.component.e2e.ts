// describe("Login", () => {
//
//   beforeEach(() => {
//     // change hash depending on router LocationStrategy
//     browser.get("/login");
//   });
//
//   it("should have a title", () => {
//     let subject = browser.getTitle();
//     let result  = "Passit";
//     expect(subject).toEqual(result);
//   });
//
//   it("forms should have correct labels", () => {
//     let emailEl = $("#userEmailLabel");
//     let passwordEl = $("#userPwLabel");
//
//     let emailResult = "Email:";
//     let pWResult = "Password:";
//
//     emailEl.isDisplayed().then(() => {
//       emailEl.getText().then((text) => {
//         expect(emailResult).toEqual(text);
//       });
//
//       passwordEl.getText().then((pwText) => {
//         expect(pWResult).toEqual(pwText);
//       });
//     });
//   });
//
//   it("user should be able to log in", () => {
//     let loginBtn = element(by.id("btn-login"));
//
//     let emailEl = $("#userEmail");
//     let passwordEl = $("#userPw");
//
//     emailEl.sendKeys("login@user.com");
//     passwordEl.sendKeys("test");
//
//     loginBtn.click();
//
//     browser.wait(() => {
//       return element(by.id("secretList")).isPresent();
//     }, 6000);
//
//     expect(element(by.id("secretList"))).toBeDefined();
//
//   });
//
//   it("should redirect to register on button click", () => {
//     let signUpBtn = element(by.id("btn-signup"));
//
//     signUpBtn.click();
//
//     browser.wait(() => {
//       return element(by.id("signup-form")).isPresent();
//     }, 6000);
//
//     expect(element(by.id("signup-form"))).toBeDefined();
//   });
//
// });
