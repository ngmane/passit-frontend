import { DebugElement } from "@angular/core";
import { ComponentFixture, TestBed } from "@angular/core/testing";
import { FormsModule } from "@angular/forms";
import { By } from "@angular/platform-browser";
import { InlineSVGModule } from "ng-inline-svg";

import { ProgressIndicatorComponent } from "../../progress-indicator";
import { LoginComponent } from "./login.component";

describe("LoginComponent", () => {
  let comp: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let de: DebugElement;
  let el: any;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgressIndicatorComponent, LoginComponent ],
      imports: [FormsModule, InlineSVGModule],
    });

    fixture = TestBed.createComponent(LoginComponent);

    comp = fixture.componentInstance;

    // query for the title <h1> by CSS element selector
    de = fixture.debugElement.query(By.css("h2"));
    el = de.nativeElement;
  });

  it("should have title", () => {
    expect(el.textContent).toContain("Log in");
  });
});
