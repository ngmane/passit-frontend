/*
* @angular
*/
import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { Store } from "@ngrx/store";
import { IState } from "../app.reducers";
import { LOG_OUT } from "../shared/app-data/authState.actions";

@Component({
  selector: "account",
  styleUrls: ["./account.component.scss"],
  templateUrl: "./account.component.html"
})

export class AccountComponent {
  constructor(private router: Router, private store: Store<IState>) {}

  /*
  * remove user from local storage to log user out
  * navigate to home
  */
  logout() {
    this.store.dispatch({ type: LOG_OUT });
    this.router.navigate(["/login"]);
  }
}
