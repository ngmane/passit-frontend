import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { InlineSVGModule } from "ng-inline-svg";

import { AccountComponent } from "./account.component";
import { ChangePasswordComponent, ChangePasswordContainer } from "./change-password";
import { ConfirmEmailComponent, ConfirmEmailContainer } from "./confirm-email";
import { DeleteComponent, DeleteContainer } from "./delete/";
import { LoginComponent, LoginContainer } from "./login";
import { RegisterComponent, RegisterContainer } from "./register";

import { UserService } from "./user";

import { ProgressIndicatorModule } from "../progress-indicator";

export { User } from "./user";

export const COMPONENTS = [
  AccountComponent,
  ChangePasswordComponent,
  ChangePasswordContainer,
  ConfirmEmailComponent,
  ConfirmEmailContainer,
  DeleteContainer,
  DeleteComponent,
  LoginComponent,
  LoginContainer,
  RegisterComponent,
  RegisterContainer,
];

export const SERVICES = [
  UserService,
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    InlineSVGModule,
    RouterModule,
    ReactiveFormsModule,
    ProgressIndicatorModule,
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
  providers: [SERVICES],
})
export class AccountModule { }
