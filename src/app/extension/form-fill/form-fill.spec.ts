import { guessAndFillLoginForm, guessLoginForm } from "./form-fill";

function skipIfPhantom() {
  let ua = window.navigator.userAgent;
  if (ua.indexOf("PhantomJS") >= 0) {
    pending();
  }
}

describe("FormFill", () => {
  beforeEach(() => {
    // Manually clean up any forms in shitmine before proceeding.
    let forms = document.getElementsByTagName("form");
    Array.prototype.forEach.call(forms, (form: any) => {
      form.parentNode.removeChild(form);
    });

    let body = document.getElementsByTagName("body")[0];
    let element = document.createElement("div");
    // It's not very good yet, just making the wut field a password causes it to score equally
    body.appendChild(element).innerHTML = `
<form name="horribleGuess">
  <input name="derp" />
</form>

<form name="almost">
  <input name="derp" />
  <input name="wut" type="text"/>
  <input type="submit">
</form>

<form name="dis_form" id="dis_form" action="javascript:void(0)">
  <input name="username" type="text" />
  <input name="password" type="password"/>
  <input type="submit" id="the-submit">
</form>
    `;
  });

  /** Find the form that is the best guess for the login form */
  it("should find a login form", () => {
    skipIfPhantom();
    let formData = guessLoginForm();
    expect(formData!.usernameField.name).toBe("username");
  });

  /** Spy on the form submit and ensure it was called
   * Using set timeout....because the guess and fill function doesn't return anything
   * maybe it should?
   */
  it("should find and fill a login form", (done) => {
    skipIfPhantom();
    let submitButton = document.getElementById("the-submit");
    spyOn(submitButton, "click");
    let data = {
      clientData: {loginUrl : document.URL, username : "username"},
      criticalData : {password : "password"}};
    guessAndFillLoginForm(data);
    setTimeout(() => {
      expect(submitButton!.click).toHaveBeenCalled();
      done();
    }, 800);
  });
});
