import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { routing, routingStore } from "../app.routing";

import { InlineSVGModule } from "ng-inline-svg";
import { TooltipModule } from "ngx-tooltip";

import { BackgroundComponent } from "./background/";
import { PopupComponent, PopupContainer, PopupHotlistComponent } from "./popup/";

import { SharedModule } from "../shared/";

export const COMPONENTS = [
  PopupComponent,
  PopupContainer,
  PopupHotlistComponent,
  BackgroundComponent,
];

@NgModule({
  imports: [
    CommonModule,
    InlineSVGModule,
    routing,
    routingStore,
    SharedModule,
    TooltipModule,
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS
})
export class ExtensionModule { }
