import {Component, OnDestroy, OnInit} from "@angular/core";

import { Store } from "@ngrx/store";
import * as fromRoot from "../../app.reducers";

@Component({
  selector: "app-background",
  template: "<h1>Background page</h1>"
})
export class BackgroundComponent implements OnInit, OnDestroy {
  constructor(
    private store: Store<fromRoot.IState>
  ) {
    let listState = store.select(fromRoot.getAuthState);
    listState.select("userToken").subscribe((text) => {
      // console.log(text);
    });
  }

  ngOnInit() {
    // this.storage.getOptions().subscribe(options => {
    //   this.ping = options.ping;
    //   this.setOnMessageListener(true);
    //   this.setOnUpdateListener(this.ping);
    // });
  }

  ngOnDestroy() {
    // this.setOnUpdateListener(false);
    // this.setOnMessageListener(false);
  }
}
