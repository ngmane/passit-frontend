// tslint:disable:max-classes-per-file
import { Action } from "@ngrx/store";

export const SET_SELECTED_SECRET = "[Popup] Set Selected Secret";
export const NULL_SELECTED_SECRET = "[Popup] Null Selected Secret";
export const SET_SEARCH = "[Popup] Set Search";

export class SetSelectedSecretAction implements Action {
  readonly type = SET_SELECTED_SECRET;

  constructor(public payload: number) { }
}

export class NullSelectedSecretAction implements Action {
  readonly type = NULL_SELECTED_SECRET;
}

export class SetSearchAction implements Action {
  readonly type = SET_SEARCH;

  constructor(public payload: string) { }
}

export type Actions
  = SetSelectedSecretAction
  | NullSelectedSecretAction
  | SetSearchAction;
