import { IDBSecret } from "passit-sdk-js/passit_sdk/interfaces";

export interface IPopupSecret extends IDBSecret {
  isSelected?: boolean;
}
