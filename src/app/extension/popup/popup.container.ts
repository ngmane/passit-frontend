// tslint:disable:variable-name
import { Component, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs/Observable";
import { combineLatest } from "rxjs/observable/combineLatest";

import * as fromRoot from "../../app.reducers";
import { filterOnSearch } from "../../secrets";
import { SecretService } from "../../secrets/secret.service";
import { copyToClipboard } from "../../utils";
import { IPopupSecret } from "./interfaces";
import { SetSearchAction, SetSelectedSecretAction } from "./popup.actions";

// TODO find a better way to get this code injected.
const formFill = `
var isVisible = function (element) {
    return (element.offsetWidth > 0 || element.offsetHeight > 0);
};
var LARGE_SCORE_VALUE = 1000;
var createFieldDict = function (fields) {
    var rval = [];
    for (var i = 0; i < fields.length; ++i) {
        var field = fields[i];
        if (field.tagName === "BUTTON") {
            var button = field;
            rval.push({
                name: button.name,
                id: button.id,
                "class": button.className,
                value: button.textContent,
                type: "button",
                pointer: field,
                itemNo: i
            });
        }
        else if (field.tagName === "A") {
            var anchor = field;
            rval.push({
                name: anchor.name || field.className,
                id: anchor.id,
                "class": anchor.className,
                type: "a",
                value: field.textContent,
                pointer: field,
                itemNo: i
            });
        }
        else {
            var input = field;
            rval.push({
                name: input.name,
                id: input.id,
                "class": input.className,
                value: (input.type === "image") ? input.alt : input.value,
                type: input.type || "text",
                maxlength: input.maxLength > -1 ? input.maxLength : 10,
                pointer: field,
                itemNo: i
            });
        }
    }
    return rval;
};
var getMaximumElement = function (elements, scoreFunc) {
    if (elements) {
        var maxScore = -1;
        var maxElement = null;
        for (var i = 0; i < elements.length; i++) {
            var score = scoreFunc(elements[i]);
            if (score > maxScore) {
                maxScore = score;
                maxElement = elements[i];
            }
        }
        return maxElement;
    }
    return null;
};
/* Score a username input field.  Field type is the most important criteria.
 * Having a non-empty value is useful to determine the username when saving a
 * form.
 */
var usernameScoreFunc = function (a, passwordFieldHint) {
    var score = 0;
    if (a.type === "text" || a.type === "email") {
        score += 2;
    }
    else {
        score -= LARGE_SCORE_VALUE;
    }
    if (passwordFieldHint) {
        // as of jquery 1.3.2, items are returned in document order, this
        // distance will tell us the number of matching elements between
        // the two items.
        var distance = passwordFieldHint.itemNo - a.itemNo;
        if (distance > 0) {
            score += 1.0 / distance;
        }
    }
    return score;
};
var guessUsernameField = function (elements, passwordFieldHint) {
    var usernameField = getMaximumElement(elements, function (a) { return usernameScoreFunc(a, passwordFieldHint); });
    // Enforce a minimum threshold to prevent really bad guesses.
    if (usernameField !== null && usernameScoreFunc(usernameField) > 0) {
        return usernameField;
    }
    else {
        return null;
    }
};
/* Score a password input field.  Field type is the most important criteria.
 * Having a non-empty value is useful to determine the username when saving a
 * form.  Non-password fields are heavily penalized.
 */
var passwordScoreFunc = function (a) {
    var score = a.value ? 1 : 0;
    var name = a.name ? a.name.toLowerCase() : "";
    if (a.type === "password") {
        // catch credit card CCV fields
        if (a.maxlength <= 4) {
            score -= LARGE_SCORE_VALUE;
        }
        else if (name.indexOf("creditcard") !== -1) {
            score -= LARGE_SCORE_VALUE;
        }
        else {
            score += 2;
        }
    }
    else {
        score -= LARGE_SCORE_VALUE;
    }
    return score;
};
var guessPasswordField = function (elements) {
    var passwordField = getMaximumElement(elements, passwordScoreFunc);
    if (passwordField !== null && passwordScoreFunc(passwordField) > 0) {
        return passwordField;
    }
    else {
        return null;
    }
};
/* we allow image submit buttons for SUBMITTING once we have chosen
 * a form, but ignore them for RANKING purposes.
 */
var guessSubmitField = function (elements, allowImageButtons, $preferAfterThisField) {
    var submitScoreFunc = function (a) {
        if (a.type === "text" || a.type === "password") {
            return -LARGE_SCORE_VALUE;
        }
        var score = (a.type === "submit" || a.type === "button") ? 1 : 0;
        // in some cases (when submitting the form) we want to match image
        // buttons
        score += (a.type === "image" && allowImageButtons) ? 0.75 : 0;
        score += (a.type === "a") ? 0.25 : 0;
        var value = a.value ? a.value.toLowerCase() : "";
        if (value.indexOf("forgot") !== -1) {
            score -= LARGE_SCORE_VALUE;
        }
        // prefer exact matches.
        // this helps avoid things like "forgot login"
        if (
        // TODO: internationalize
        (value === "sign in") ||
            (value === "log in") ||
            (value === "log on") ||
            (value === "submit") ||
            (value === "login") ||
            (value === "go")) {
            score += 2;
        }
        else if ((value.indexOf("sign in") !== -1) ||
            (value.indexOf("log in") !== -1) ||
            (value.indexOf("log on") !== -1) ||
            (value.indexOf("submit") !== -1) ||
            (value.indexOf("login") !== -1) ||
            (value.indexOf("go") !== -1)) {
            score += 1;
        }
        if ($preferAfterThisField) {
            var BITMASK = 4; // Node.DOCUMENT_POSITION_FOLLOWING is defined as 4 but not in node.
            // tslint:disable-next-line:no-bitwise
            var isAfter = $preferAfterThisField.compareDocumentPosition(a.pointer) & BITMASK;
            score += (isAfter) ? 1 : -1;
        }
        return score;
    };
    var submitField = getMaximumElement(elements, submitScoreFunc);
    if (submitField !== null && submitScoreFunc(submitField) > 0) {
        return submitField;
    }
    else {
        return null;
    }
};
/** Returns a login form dict if input form is a login form, or null otherwise.
 * Setting requireFieldVisibility only considers visible form fields when
 * looking for username/password/submit fields (default: true).
 */
var getLoginForm = function (form, requireFieldVisibility) {
    if (requireFieldVisibility === void 0) { requireFieldVisibility = true; }
    var fieldNodes = form.querySelectorAll("input:not([type=hidden]),button:not([type=hidden]),a");
    var fields = Array.prototype.slice.call(fieldNodes, 0);
    if (requireFieldVisibility) {
        fields = Array.prototype.filter.call(fields, function (field) { return isVisible(field); });
    }
    var fieldsRecord = createFieldDict(fields);
    var passwordField = guessPasswordField(fieldsRecord);
    var usernameField = null;
    var goAhead = !!passwordField;
    if (passwordField) {
        usernameField = guessUsernameField(fieldsRecord, passwordField);
    }
    goAhead = goAhead && (usernameField !== null);
    if (goAhead) {
        var submitField = guessSubmitField(fieldsRecord, true, usernameField && usernameField.pointer);
        var fieldDict = createFieldDict([(passwordField ? passwordField : usernameField).pointer.closest("form")]);
        // we need to find a form.
        if (!fieldDict) {
            return null;
        }
        return { usernameField: usernameField,
            passwordField: passwordField,
            submitField: submitField,
            allFields: fieldsRecord,
            formDict: fieldDict[0],
            id: form.id };
    }
    else {
        return null;
    }
};
var LOGIN_BUTTON_LABELS = ["login", "log in", "log on", "signin", "sign in"];
var SIGNUP_BUTTON_LABELS = ["signup", "sign up", "join", "create", "register", "start", "free", "trial"];
/* Score the submit button as */
var loginSubmitButtonScoreFunc = function (field) {
    var submitScore = 0;
    if (field && field.value) {
        var label = field.value.toLowerCase();
        var i = void 0;
        var s = void 0;
        for (i = 0; i < LOGIN_BUTTON_LABELS.length; ++i) {
            s = LOGIN_BUTTON_LABELS[i];
            if (label.indexOf(s) !== -1) {
                submitScore += 2;
            }
        }
        for (i = 0; i < SIGNUP_BUTTON_LABELS.length; ++i) {
            s = SIGNUP_BUTTON_LABELS[i];
            if (label.indexOf(s) !== -1) {
                submitScore -= LARGE_SCORE_VALUE;
            }
        }
    }
    return submitScore;
};
var LOGIN_FORM_ID_STRINGS = ["login", "signin"];
var SIGNUP_FORM_ID_STRINGS = ["signup", "regist"];
var loginIdScoreFunc = function (formId) {
    var idScore = 0;
    if (formId) {
        var id = formId.toLowerCase();
        var i = void 0;
        for (i = 0; i < LOGIN_FORM_ID_STRINGS.length; ++i) {
            var s = LOGIN_FORM_ID_STRINGS[i];
            if (id.indexOf(s) !== -1) {
                idScore += 1;
            }
        }
        // sometimes forms have both signup and signin.  we should not punish those forms.
        if (idScore === 0) {
            for (i = 0; i < SIGNUP_FORM_ID_STRINGS.length; ++i) {
                var s = SIGNUP_FORM_ID_STRINGS[i];
                if (id.indexOf(s) !== -1) {
                    idScore -= LARGE_SCORE_VALUE;
                }
            }
        }
    }
    return idScore;
};
var countFieldsOfType = function (fields, type) {
    var count = 0;
    for (var _i = 0, fields_1 = fields; _i < fields_1.length; _i++) {
        var field = fields_1[_i];
        if (field.type === type) {
            ++count;
        }
    }
    return count;
};
var loginFormScoreFunc = function (formDict) {
    if (!formDict) {
        return 0;
    }
    var score = 0;
    if (formDict.usernameField) {
        score += usernameScoreFunc(formDict.usernameField);
    }
    if (!formDict.passwordField && formDict.usernameField.emptyPasswordPermitted) {
        /* ? */
    }
    else {
        score += passwordScoreFunc(formDict.passwordField);
    }
    score += loginSubmitButtonScoreFunc(formDict.submitField);
    score += loginIdScoreFunc(formDict.id);
    // Prevents matches for signup and change password forms.
    if (countFieldsOfType(formDict.allFields, "password") > 1) {
        score -= LARGE_SCORE_VALUE;
    }
    return score;
};
/* Finds best match for a login form in the current document, or null if no
 * login form is found.
 */
var guessLoginForm = function (hints) {
    var scoringFunction = loginFormScoreFunc;
    var formSelection = Array.prototype.slice.call(document.querySelectorAll("form"));
    var forms = formSelection.map(function (form) {
        return getLoginForm(form);
    });
    var loginForm = getMaximumElement(forms, scoringFunction);
    if (loginForm && scoringFunction(loginForm) < 0) {
        return null;
    }
    return loginForm;
};
var getCanonicalHost = function (fullUrl) {
    return "http://stub.com";
};
var fillLoginForm = function (formData) {
    var formDomain = getCanonicalHost(formData.loginUrl);
    var pageDomain = getCanonicalHost(document.URL);
    if (formDomain !== pageDomain) {
        throw "Domain mismatch: " + pageDomain + ", expected: " + formDomain;
    }
    var un = (formData.usernameField) ? formData.usernameField.value : null;
    var pw = (formData.passwordField) ? formData.passwordField.value : null;
    // forms sometimes load strangely.
    // TODO: Which ones? What does that mean exactly?
    // Lastpass can cause conflicts: previously we filled the form then delayed submit()
    // lastpass would fill the form in the middle, causing a login for the wrong account.
    setTimeout(function () {
        // Find the form with the username / password
        var sendKeyEvents = function (inputId) {
            // TODO: figure out how to enable this without horrible performance problems
            var event = new KeyboardEvent("keydown");
            event.key = 13;
            document.querySelector("#" + inputId).dispatchEvent(event);
            event = new KeyboardEvent("keypress");
            event.key = 13;
            document.querySelector("#" + inputId).dispatchEvent(event);
            event = new KeyboardEvent("keyup");
            event.key = 13;
            document.querySelector("#" + inputId).dispatchEvent(event);
        };
        var $user_input = (formData.usernameField ? formData.usernameField.pointer : formData.usernameField);
        if ($user_input) {
            var $user_form = $user_input.closest("form");
            $user_input.value = un;
            var userInputId_1 = $user_input.id;
            if (!userInputId_1) {
                userInputId_1 = "MITRO____184379378465893";
                $user_input.setAttribute("id", userInputId_1);
            }
            setTimeout(function () {
                sendKeyEvents(userInputId_1);
            }, 10);
        }
        if (formData.passwordField) {
            var $pass_input = formData.passwordField.pointer;
            // assert($pass_input.attr("type") === "password");
            var $pass_form = $pass_input.closest("form");
            $pass_input.value = pw;
            // HACK
            // helper.preventAutoFill($pass_input, $pass_form);
            // simulate keypresses
            var passInputId_1 = $pass_input.getAttribute("id");
            if (!passInputId_1) {
                passInputId_1 = "MITRO____184379378465894";
                $pass_input.setAttribute("id", passInputId_1);
            }
            setTimeout(function () {
                sendKeyEvents(passInputId_1);
            }, 20);
            // This call will disable autocomplete and the browser won't offer to save
            // the password on the browser's secure area
            // Tested in Chrome and Firefox
            $pass_form[0].setAttribute("autocomplete", "off");
            if ($user_input) {
                $user_input.setAttribute("autocomplete", "off");
            }
            $pass_input.setAttribute("autocomplete", "off");
        }
        // Simulate implicit form submission (e.g. pressing enter)
        // Click the first submit button. Submit buttons are:
        // - <input type="submit">
        // - <input type="image">
        // - <button> (with type="submit" or no type)
        // selecting buttons without type attribute is tricky; do that in a separate query
        setTimeout(function () {
            var submit_button = formData.submitField;
            if (submit_button) {
                try {
                    // sometimes the code runs validators and the submit button is disabled
                    submit_button.pointer.removeAttribute("disabled");
                }
                catch (e) {
                    console.log(e);
                }
                submit_button.pointer.click();
            }
            else {
                // this may work for forms without submit buttons (e.g. buttons with onclick)
                try {
                    // if form has input[name="submit"] it shadows submit() method causing TypeError
                    $pass_form[0].submit();
                }
                catch (e) {
                    if (e instanceof TypeError) {
                        try {
                            // console.log('form.submit not a method; trying to click');
                            $pass_form[0].submit.click();
                        }
                        catch (e2) {
                            // console.log('trying to click on the submit button');
                            var $submit_thing = $user_form.querySelector(":submit");
                            $submit_thing.click();
                        }
                    }
                }
            }
        }, 300);
    }, 200);
};
var guessAndFillLoginForm = function (username, password) {
    formData = { clientData: { loginUrl: document.URL, "username": username }, criticalData: { "password": password } };
    var loginForm = guessLoginForm(formData);
    if (!loginForm) {
        console.log('login form not found');
        return false;
    }
    loginForm.loginUrl = formData.clientData.loginUrl;
    if (loginForm.usernameField) {
        loginForm.usernameField.value = formData.clientData.username;
    }
    if (loginForm.passwordField) {
        loginForm.passwordField.value = formData.criticalData.password;
    }
    fillLoginForm(loginForm);
    return true;
};
`;

/** Open the url and log in by guessing what the form is.
 * This function only works in the extension
 */
const logIntoSite = (url: string, username: string, password: string) => {
  return browser.tabs.create({
    url,
  }).then((tab) => {
    // TODO probably not safe
    // Crappy sanity check to limit damage on injection attack
    username = username.substring(0, 100);
    password = password.substring(0, 100);
    let injectedCode = formFill + ` guessAndFillLoginForm("${username}", "${password}");`;
    return browser.tabs.executeScript(tab.id, {
      code: injectedCode,
    });
  });
};

@Component({
  selector: "app-popup-container",
  template: `
    <app-popup
      [secrets]="secrets$ | async"
      [search]="search$ | async"
      (setSelected)="setSelected($event)"
      (searchUpdate)="searchUpdate($event)"
      (onSignIn)="signIn($event)"
      (onCopyUsername)="copyUsername($event)"
      (onCopyPassword)="copyPassword($event)"
      (onDetail)="openDetails($event)"
      (openFullApp)="openFullApp()"
    ></app-popup>
  `
})
export class PopupContainer implements OnInit {
  secrets$: Observable<IPopupSecret[]>;
  search$: Observable<string>;

  constructor(private store: Store<fromRoot.IState>, private secretService: SecretService) {
    let popupSelected = store.select(fromRoot.getPopupSelected);
    this.search$ = store.select(fromRoot.getPopupSearch);

    const getFilterdAndSelected = (secrets: IPopupSecret[], searchTerm: string, secretManaged?: number) => {
      let filtered = filterOnSearch(secrets, searchTerm, secretManaged);
      return filtered.map((_secret) => {
        let secret: IPopupSecret = Object.assign({}, _secret, {
          isSelected: false,
        });
        if (secret.id === secretManaged) {
          secret.isSelected = true;
        }
        return secret;
      });
    };

    this.secrets$ = Observable.combineLatest(
      store.select(fromRoot.getSecrets),
      store.select(fromRoot.getPopupSearch),
      popupSelected,
      getFilterdAndSelected,
    );
  }

  ngOnInit() {
    // TODO make this not happen EVERY time
    this.getSecrets();
  }

  getSecrets() {
    this.secretService.getSecrets();
  }

  setSelected(selected: number) {
    this.store.dispatch(new SetSelectedSecretAction(selected));
  }

  searchUpdate(term: string) {
    this.store.dispatch(new SetSearchAction(term));
  }

  openFullApp() {
    browser.tabs.create({
      url: "/index.html"
    });
    window.close();
  }

  copyUsername(secret: IPopupSecret) {
    copyToClipboard(secret.data["username"]);
  }

  copyPassword(secret: IPopupSecret) {
    this.secretService.showSecret(secret).then((decrypted) => {
      copyToClipboard(decrypted["password"]);
    });
  }

  openDetails(secret: IPopupSecret) {
    browser.tabs.create({
      url: "/index.html#/list;id=" + secret.id,
    });
    window.close();
  }

  signIn(secret: IPopupSecret) {
    let url = secret.data["url"];
    let username = secret.data["username"];
    this.secretService.showSecret(secret).then((decrypted) => {
      let password = decrypted["password"];
      logIntoSite(url, username, password).then(() => {
        window.close();
      });
    });
  }
}
