import { Component, EventEmitter, Input, Output } from "@angular/core";

import { IDBSecret } from "passit-sdk-js/passit_sdk/interfaces";

@Component({
  selector: "app-popup-hotlist",
  templateUrl: "./popup-hotlist.component.html",
  styleUrls: ["./popup-hotlist.component.scss"],
})
export class PopupHotlistComponent {
  @Input() secret: IDBSecret;
  @Output() onSignIn = new EventEmitter();
  @Output() onCopyUsername = new EventEmitter();
  @Output() onCopyPassword = new EventEmitter();
  @Output() onDetail = new EventEmitter();
}
