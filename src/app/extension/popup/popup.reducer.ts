import {
  Actions,
  NULL_SELECTED_SECRET,
  SET_SEARCH,
  SET_SELECTED_SECRET,
} from "./popup.actions";

export interface IPopupState {
  selectedSecret: number | null;
  search: string;
  lastOpened: string | null;
}

export const initialState: IPopupState = {
  selectedSecret: null,
  search: "",
  lastOpened: null,
};

export function popupReducer(state = initialState, action: Actions): IPopupState {
  switch (action.type) {
    case SET_SELECTED_SECRET:
      return Object.assign({}, state, {
        selectedSecret: action.payload,
        lastOpened: new Date().toString(),
      });

    case NULL_SELECTED_SECRET:
      return Object.assign({}, state, {
        selectedSecret: initialState.selectedSecret,
        lastOpened: new Date().toString(),
      });

    case SET_SEARCH:
      return Object.assign({}, state, {
        search: action.payload,
        lastOpened: new Date().toString(),
      });

    default:
      return state;
  }
}

export const getSelectedSecret = (state: IPopupState) => state.selectedSecret;
export const getSearch = (state: IPopupState) => state.search;
