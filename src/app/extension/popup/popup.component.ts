import { Component, EventEmitter, Input, Output } from "@angular/core";

import { IDBSecret } from "passit-sdk-js/passit_sdk/interfaces";

@Component({
  selector: "app-popup",
  templateUrl: "./popup.component.html",
  styleUrls: ["./popup.component.scss"]
})
export class PopupComponent {
  @Input() secrets: IDBSecret;
  @Input() search: string;
  @Output() setSelected = new EventEmitter<number>();
  @Output() searchUpdate = new EventEmitter<string>();
  @Output() openFull = new EventEmitter();
  @Output() onSignIn = new EventEmitter<IDBSecret>();
  @Output() onCopyUsername = new EventEmitter<IDBSecret>();
  @Output() onCopyPassword = new EventEmitter<IDBSecret>();
  @Output() onDetail = new EventEmitter<IDBSecret>();
  @Output() openFullApp = new EventEmitter();
}
