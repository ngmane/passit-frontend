/* tslint:disable:variable-name */
import { IDBSecret, INewSecret, ISecret, ISecretData, IVisibleData } from "passit-sdk-js/passit_sdk/interfaces";

/**
 * Secret model. Constructor accepts both ISecret and IDBSecret
 * and transforms these simple data types into a complex object with
 * handy functions like get password
 */
export class Secret implements INewSecret {
  public id?: number;
  public name?: string;
  public type?: string;
  public secrets?: ISecretData;
  public group_id?: any;
  public group_name?: any;
  public dbSecret?: IDBSecret;
  public visible_data?: IVisibleData;

  /** Pass existing secret like object, if not is provided
   * a new secret like object is created.
   */
  constructor(public polySecret?: IDBSecret | ISecret) {
    // These match both types so let's set in any case
    if (polySecret) {
      this.id = polySecret.id;
      this.name = polySecret.name;
      this.type = polySecret.type;

      // Determine type
      if ((polySecret as IDBSecret).secret_through_set) {
        let dbSecret = polySecret as IDBSecret;
        this.secrets = dbSecret.secret_through_set;
        this.visible_data = dbSecret.data;
        this.dbSecret = dbSecret;
      } else {
        let secret = polySecret as ISecret;
        this.visible_data = secret.visible_data;
        this.secrets = secret.secrets;
      }
    } else {
      // Initialize new object with expected data fields
      this.visible_data = {};
      this.secrets = {};
    }
  }
}
