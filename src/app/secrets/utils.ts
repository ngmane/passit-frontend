import { IDBSecret } from "passit-sdk-js/passit_sdk/interfaces";

/**
 * Filter a list of secrets based on a search term
 * Set secretManaged to ensure it always shows
 */
export const filterOnSearch = (secrets: IDBSecret[], searchTerm: string, secretManaged?: number) => {
  if (!searchTerm) {
    return secrets;
  }
  let term = searchTerm.toLowerCase();
  return secrets.filter((secret) => {
    let name = secret.name;
    if (name) {
      name = name.toLowerCase();
    }
    let username: any = secret.data["username"];
    if (username) {
      username = username.toLowerCase();
    } else {
      username = "";
    }
    return username.indexOf(term) >= 0 ||
      secret.id === secretManaged ||
      name.indexOf(term) >= 0;
  });
};
