import { ActionReducer } from "@ngrx/store";
import * as secret from "./secret.actions";

import { IDBSecret } from "passit-sdk-js/passit_sdk/interfaces";

export interface ISecretState {
  secrets: IDBSecret[];
}

const initialState: ISecretState = {
  secrets: [],
};

export function secretReducer(state = initialState, action: secret.Actions): ISecretState {
  switch (action.type) {
    case secret.ADD_SECRET:
      return Object.assign({}, state, {
        secrets: [
          ...state.secrets,
          action.payload,
        ]
      });

    case secret.REMOVE_SECRET:
      let secrets = state.secrets.filter((secret) => {
        return (secret.id !== action.payload);
      });
      return Object.assign({}, state, {
        secrets
      });

    case secret.REPLACE_SECRET:
      let index = state.secrets.findIndex((secret) => {
        return (secret.id === action.payload.id);
      });
      let updatedSecrets = state.secrets.slice();
      updatedSecrets[index] = action.payload;
      return Object.assign({}, state, {
        secrets: updatedSecrets
      });

    case secret.SET_SECRETS:
      return Object.assign({}, state, {
          secrets: action.payload,
      });

    default:
      return state;
  }
}

export const getSecrets = (state: ISecretState) => state.secrets;
