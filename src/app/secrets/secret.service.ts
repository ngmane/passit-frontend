/*
* @angular
*/
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Store } from "@ngrx/store";

/*
* Passit
*/
import * as fromRoot from "../app.reducers";
import { NgPassitSDK } from "../ngsdk/sdk";
import { Secret } from "../secrets";
import * as secret from "../secrets/secret.actions";
import { LOG_OUT } from "../shared/app-data/authState.actions";

import { IDBSecret, IDBSecretThrough, INewSecret, ISecret } from "passit-sdk-js/passit_sdk/interfaces";

@Injectable()
export class SecretService {
  constructor(public sdk: NgPassitSDK, private store: Store<fromRoot.IState>, private router: Router) {}

  /*
  * decrypt recieving just id number not whole object
  */
  public copySecret(secretId: number) {
    return this.sdk.get_secret(secretId)
      .then((data) => {
        return this.sdk.decrypt_secret(data)
          .then((resp) => {
            return resp;
          });

      })
      .catch((err) => console.error(err));
  }

  /*
  * send create secret post to sdk
  */
  public createSecrets(data: INewSecret) {
    return this.sdk.create_secret(data)
    .then((resp) => {
      return resp;
    })
    .catch((err) => {
      console.error(err);
    });

  }

  /*
  * delete seccret
  */
  public deleteSecret(secretId: number): Promise<any> {
    return this.sdk.delete_secret(secretId)
      .then((resp) => {
        return resp;
      })
      .catch((err) => {
        console.error(err);
      });
  }

  /*
  * get secrets
  */
  public getSecrets(): Promise<void> {
    return this.sdk.list_secrets()
      .then((resp) => {
        this.store.dispatch(new secret.SetSecretsAction(resp));
      })
      .catch((err) => {
        console.error(err);
        if (err.res && err.res.status === 401) {
          this.store.dispatch({ type: LOG_OUT });
        }
        if (err.res.json().detail === "User's email is not confirmed.") {
          this.router.navigate(["/confirm-email"]);
        }
      });
  }

  /*
  * get secret from id
  * then decrypt secret
  * so user can see unencrypted secret
  */
  public showSecret(secret: IDBSecret) {
    return this.sdk.decrypt_secret(secret)
      .then((resp) => resp)
      .catch((err) => {
        console.error(err);
      });
  }

  /*
  * edit/update secret
  */
  public updateSecret(secret: ISecret): Promise<IDBSecret> {
    return this.sdk.update_secret(secret)
      .then((resp) => resp)
      .catch((err) => {
        console.error(err);
        return err;
      });
  }

  /** Compared selected groups with existing groups from the database
   * Then makes changes to make the secret in sync
   * Promise resolves when all changes are finished.
   * Changes happen asyncronously and are not transactional at this time
   */
  public updateGroupsForSecret(groupIds: number[], secretId: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.sdk.get_secret(secretId).then((secret) => {
        let existingGroupIds = [];
        for (let through of secret.secret_through_set) {
          if (through.group) {
            existingGroupIds.push(through.group);
          }
        }
        let promises = [];
        for (let groupId of groupIds) {
          // if new group not in existing group ids
          if (existingGroupIds.indexOf(groupId) < 0 ) {
            promises.push(this.sdk.add_group_to_secret(groupId, secretId));
          }
        }
        for (let groupId of existingGroupIds) {
          // if existing group id not in new group ids.
          if (groupIds.indexOf(groupId) < 0 ) {
            let secretThrough = secret.secret_through_set.find((through) => through.group === groupId);
            promises.push(this.sdk.remove_group_from_secret(secretId, secretThrough!.id!));
          }
        }
        Promise.all(promises).then(() => {
          resolve();
        });
      });
    });
  }

  /**
   * Takes existing secrets that were just decrypted and assigns them to the
   * object that the secret form uses
   */
  public setPlaintextSecrets = (secrets: any) => {
    const populatedSecrets = {};
    Object.keys(secrets).forEach((secret) => populatedSecrets[secret] = secrets[secret]);
    return populatedSecrets;
  }

  /**
   * Takes fields from the secret form that are meant to be encrypted and
   * adds them to an object that gets attached if there is a non-empty value.
   */
  public getPlaintextSecrets = (fields: string[], secrets: any) => {
    // Only include secret keys if they were changed
    const secretsToExport = {};
    fields.forEach((field) => {
      if (secrets[field] && secrets[field] !== "") {
        secretsToExport[field] = secrets[field];
      }
    });
    return secretsToExport;
  }
}
