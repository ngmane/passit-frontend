import { Action } from "@ngrx/store";

import { IDBSecret, IGroup } from "passit-sdk-js/passit_sdk/interfaces";

export const ADD_SECRET = "Add Secret";
export const REMOVE_SECRET = "Remove Secret";
export const REPLACE_SECRET = "Replace secret";
export const SET_SECRETS = "Set Secrets";

export class AddSecretAction implements Action {
  readonly type = ADD_SECRET;

  constructor(public payload: IDBSecret) { }
}

export class RemoveSecretAction implements Action {
  readonly type = REMOVE_SECRET;

  constructor(public payload: number) { }
}

export class ReplaceSecretAction implements Action {
  readonly type = REPLACE_SECRET;

  constructor(public payload: IDBSecret) { }
}

export class SetSecretsAction implements Action {
  readonly type = SET_SECRETS;

  constructor(public payload: IDBSecret[]) { }
}

export type Actions
  = AddSecretAction
  | RemoveSecretAction
  | ReplaceSecretAction
  | SetSecretsAction;
