export { Secret } from "./secret";
export { filterOnSearch } from "./utils";

/* tslint:disable:max-line-length */
export const dbSecretExample = {
  data: {
    url: "1",
    username: "1"
  },
  id: 1,
  name: "1",
  secret_through_set: [
    {
      data: {
        password: "gAAAAABYZcEkAAECAwQFBgcICQoLDA0OD9NctFMhM6xZIdZAwGRqM0j7fvs3NGpzT0e9NOGBBP3DKvWvzXqGeJTwlTVrVFcp_w=="
      },
      group: 9,
      id: 12,
      is_mine: true,
      key_ciphertext: "qLpaCIHjxVdWtDpaUEfMZujQ0BTYn0z/pDvM+nLVwehrYUywZxIvUXqC2uYmkEln1uoIN+iod3VzHqyPeueErOkLu9NFA3BBWs+Kqf1Rz0ncyFtB3CZccDumU7iabBAJssy/lZvo3BtoiSqR5geqAidddGDJPtyZ9yBeMkejyZm8KKGeLtyO00aaKVCqjZQwN0LFIfxwsCuDUc6Du2WjCcl2CIN8OayPti9EWz+5qAzAEoHkyDlK0Nqq+xPE/tuFwOd2kxsv7ZfvjCprikmqgSoz1xs1ZareyNU6dytU6bR+1zoN91ri1fQPHL43ybRNQEJwVYU1U1RvH8bOjKMmvnG1XCbuwB2OpVdvm0S2fZykB4MnFshIfhtDT6uCUM8umXyMcsBNLguhRswdn9S4+9TPzISSSyhP4ltw/dfk/l+Z5YRef/66rtR6EbzUwizPXtpAgBl0XJ51JUgspS/hHAQF3m5hKKOGusG6H4IARBbaxqjgBe2N1MaBn/UkLZeT/MPIsk/E0CS57PZR0+OzRl24N1KfnBXLykwUC3kxRuw6Zo/z9261/weYqeSjymOb3DPrRvTLOlUa785HJja2fpfg3ccZi/66AMzL/R4OeS3v/3KtTBUJ9YNgSE45JoXlJrxawWV+hE4GknQ/fj2HK1gTB2lyoHfZ5Sld/1ml+8w=",
      public_key: "-----BEGIN RSA PUBLIC KEY-----\nMIICCgKCAgEAuImPPDFUkiayv9mq/XYgtvr8kPUiToTF8Yb5fmCHUwg5JB85xIZa\nMljO4TPK1vPSmcgQF2KO06X87FJJRjBy41CU5QkJV2/dJFxkmAVIHmvh3Lb6OX5D\nGr31NI6Hz6vKzI4P/bauC/yvgUmUMjyZ89+xW4niA24h3FZGxG3DQUoBuCG9gwfW\nXjDbCIaYmeeus35t6kIWqBnlnh3QIE1U/dmietL1rxZTFL/IAtNPcdbv72LlRU7w\nJHnajI/BlvcJWB9coMQk48yf1o+0eDKzvjbG4xeHSA6gf7fXb2/12U+mFwPPukVe\nPQkJ6aqzRNiUmQqvV+O93jPHAtTX7Wobf2wLgqNkhTWnJyvhLuGkPrma59cYMllF\nSaxgMDFD3ZHigQHt4t5HEt84Jdfw0jq73WjH/DuJIwKa63jSvWG/8MssGQFEwYWs\nPlutjMAHU+CGLsKSBHxKySX2kmvW4UbxL4Whgpp//NyQCpiWEs6lu8um836WcQ4J\nP6pQVleo8VXt98ilMb4wYdsEQPYh5C7y8obTCpntgAizVIey53YXoqDdsGo9fdW1\nGhZ+NmgCxkSuIXxamdyzXJ+SNJATpBpRv22DfKw/aJhZ5FMH6LQbNwwz5TdE3VJk\n+ITM/BjG+xfeG8iQgrWe1r5eDU+Y4jIY4AMrkCpYfU02pB4AZXa9zLMCAwEAAQ==\n-----END RSA PUBLIC KEY-----",
    }
  ],
  type: "website",
};
