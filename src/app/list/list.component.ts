import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  SimpleChange,
  SimpleChanges,
  ViewChild } from "@angular/core";

import { IDBSecret, IGroup, ISecret } from "passit-sdk-js/passit_sdk/interfaces";
import { IS_EXTENSION } from "../constants";
import { Secret } from "../secrets";

@Component({
  selector: "secret-list-component",
  styleUrls: ["./list.component.scss"],
  templateUrl: "./list.component.html",
})
export class SecretListComponent {
  secretAnimated = true; // Default to true in case it starts in open state
  animateAddForm = false;
  changedSecretManaged: SimpleChange;

  @Input() secrets: IDBSecret[];
  @Input() secretManaged: number;
  @Input() showCreate: boolean;
  @Input() groups: IGroup[];
  @Input() finishedUpdating: boolean;
  @Input() secretIdFromUrl: number;

  @Output() secretAddSave = new EventEmitter<{secret: ISecret, groups: number[]}>();
  @Output() secretWasSelected = new EventEmitter();
  @Output() saveSecret = new EventEmitter<{secret: ISecret, groups: number[]}>();
  @Output() hideAddSecretForm = new EventEmitter();
  @Output() showAddSecretForm = new EventEmitter();
  @Output() deleteSecret = new EventEmitter<ISecret>();
  @Output() searchUpdate = new EventEmitter<string>();

  @ViewChild("secretList") secretList: ElementRef;

  ngOnChanges(changes: SimpleChanges) {
    const changedSecretManaged = changes["secretManaged"];

    if (changedSecretManaged) {
      this.changedSecretManaged = changedSecretManaged;
      this.scrollActiveSecretRowIntoView();
    }
  }

  ngAfterViewInit() {
    this.scrollActiveSecretRowIntoView();
  }

  scrollActiveSecretRowIntoView() {
    if (this.changedSecretManaged &&
        typeof this.changedSecretManaged.currentValue === "number" &&
        this.changedSecretManaged.currentValue !== this.changedSecretManaged.previousValue) {
      const secretManagedElement =
        this.secretList.nativeElement.querySelector("#secret-list-item--" + this.changedSecretManaged.currentValue);
      if (secretManagedElement) {
        if (this.secretIdFromUrl === this.changedSecretManaged.currentValue) {
          secretManagedElement.scrollIntoView({behavior: "smooth"});
        } else {
          setTimeout(() => secretManagedElement.scrollIntoView({behavior: "smooth"}), 200);
        }
      }
    }
  }

  hideAdd() {
    this.hideAddSecretForm.emit();
    this.animateAddForm = false;
  }

  showAdd() {
    this.showAddSecretForm.emit();
    setTimeout(() => this.animateAddForm = true, 0);
  }

  /*
  * receive secret from child when secret clicked
  */
  onSecretWasSelected(secret: Secret) {
    this.secretAnimated = false;
    if (this.secretManaged !== secret.id) {
      setTimeout(() => {
        this.secretAnimated = true;
      }, 1);
    }
    this.hideAddSecretForm.emit();
    this.secretWasSelected.emit(secret);
  }
}
