import { Action, ActionReducer } from "@ngrx/store";
import * as list from "./list.actions";

export interface IListState {
  searchText: string;
  secretManaged?: number;
  showCreate: boolean;
}

const initialState: IListState = {
  searchText: "",
  showCreate: false,
};

export function ListReducer(state = initialState, action: Action | list.Actions): IListState {
  switch (action.type) {
    case list.CLEAR_MANAGED_SECRET:
      return Object.assign({}, state, {
        secretManaged: null,
      });

    case list.SET_MANAGED_SECRET:
      return Object.assign({}, state, {
        secretManaged: (action as list.SetManagedSecret).payload,
      });

    case list.SET_SEARCH_TEXT:
      return Object.assign({}, state, {
        searchText: (action as list.SetSearchText).payload,
      });

    case list.SHOW_CREATE:
      return Object.assign({}, state, {
        showCreate: true,
      });

    case list.HIDE_CREATE:
      return Object.assign({}, state, {
        showCreate: false,
      });

    case "[Router] Update Location":
      return Object.assign({}, state, {
        secretManaged: null,
        showCreate: false,
        searchText: null,
      });

    default:
      return state;
  }
}

export const getListSecretManaged = (state: IListState) => state.secretManaged;
export const getListShowCreate = (state: IListState) => state.showCreate;
