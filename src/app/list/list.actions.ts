/* tslint:disable:max-classes-per-file */
// tslint:disable:object-literal-sort-keys
import { Action } from "@ngrx/store";

export const CLEAR_MANAGED_SECRET = "clear managed secret";
export const SET_MANAGED_SECRET = "Set managed secret";
export const SET_SEARCH_TEXT = "Set search text";
export const SHOW_CREATE = "Show create secret";
export const HIDE_CREATE = "Hide create secret";

export class ClearManagedSecret implements Action {
  readonly type = CLEAR_MANAGED_SECRET;
}

export class SetManagedSecret implements Action {
  readonly type = SET_MANAGED_SECRET;

  constructor(public payload: number) { }
}

export class SetSearchText implements Action {
  readonly type = SET_SEARCH_TEXT;

  constructor(public payload: string) { }
}

export class ShowCreate implements Action {
  readonly type = SHOW_CREATE;
}

export class HideCreate implements Action {
  readonly type = HIDE_CREATE;
}

export type Actions
  = ClearManagedSecret
  | SetManagedSecret
  | SetSearchText
  | ShowCreate
  | HideCreate;
