import { Component, EventEmitter, Input, Output } from "@angular/core";
import { Form } from "@angular/forms";

import { INewSecret } from "passit-sdk-js/passit_sdk/interfaces";
import { IGroup } from "passit-sdk-js/passit_sdk/interfaces";
import { Secret } from "../../secrets/secret";

@Component({
  selector: "secret-add",
  styleUrls: ["../secret-row/secret-row.component.scss", "../list.component.scss"],
  template: `
  <h2 class="secret__title secret__title--add">
    Add New Password
  </h2>

  <secret-form
    [groups]="groups"
    (hideAddSecretForm)="hideAddSecretForm.emit()"
    (saveSecret)="save.emit($event)"
    class="secret-form"
    [class.secret-form--is-active]="secretAnimated"
  ></secret-form>
  `,
})
export class SecretAddComponent {
  secretAnimated = false;
  @Input() groups: IGroup[];
  @Output() hideAddSecretForm = new EventEmitter();
  @Output() save = new EventEmitter();

  ngOnInit() {
    setTimeout(() => this.secretAnimated = true, 10);
  }
}
