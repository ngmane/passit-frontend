import { ChangeDetectionStrategy, Component, EventEmitter } from "@angular/core";

import { IS_EXTENSION } from "../../constants";
import { Secret } from "../../secrets/secret";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  inputs: ["secret", "active"],
  outputs: ["onSecretSelected"],
  selector: "secret-row",
  styleUrls: ["./secret-row.component.scss"],
  templateUrl: "./secret-row.component.html"
})

export class SecretRowComponent {
  public active: boolean;
  public onSecretSelected: EventEmitter<Secret>;
  public secret: Secret;

  constructor() {
    this.onSecretSelected = new EventEmitter();
  }

  public toggleView(onlyWhenClosed: boolean = false) {
    // One of our toggles only needs to work when the secret is closed;
    // the other one acts as a true toggle
    if ((onlyWhenClosed && !this.active) || !onlyWhenClosed) {
      this.onSecretSelected.emit(this.secret);
    }
  }
}
