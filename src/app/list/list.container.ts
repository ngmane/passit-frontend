import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs/Rx";

import * as fromRoot from "../app.reducers";
import { GroupService } from "../group";
import * as list from "../list/list.actions";
import { filterOnSearch, Secret } from "../secrets";
import * as secretActions from "../secrets/secret.actions";
import { SecretService } from "../secrets/secret.service";

import { IDBSecret, IGroup, ISecret } from "passit-sdk-js/passit_sdk/interfaces";

@Component({
  selector: "secret-list-container",
  styleUrls: ["./list.component.scss"],
  template: `
    <secret-list-component
      [secrets]="secrets$ | async"
      [secretManaged]="secretManaged"
      [showCreate]="showCreate$ | async"
      [groups]="groups$ | async"
      [finishedUpdating]="finishedUpdating"
      [secretIdFromUrl]="secretIdFromUrl"
      (secretAddSave)="secretAddSave($event)"
      (secretWasSelected)="secretWasSelected($event)"
      (hideAddSecretForm)="hideAddSecretForm()"
      (showAddSecretForm)="showAddSecretForm()"
      (saveSecret)="saveSecret($event)"
      (deleteSecret)="deleteSecret($event)"
      (searchUpdate)="searchUpdate($event)"
    ></secret-list-component>
  `
})
export class SecretListContainer implements OnInit {
  showCreate$: Observable<boolean>;
  secrets$: Observable<IDBSecret[]>;
  secretManaged: number;
  groups$: Observable<IGroup[]>;
  finishedUpdating = false;
  secretIdFromUrl: number | null = null;

  constructor(
    private secretService: SecretService,
    private groupService: GroupService,
    private store: Store<fromRoot.IState>,
    private route: ActivatedRoute,
  ) {
    this.showCreate$ = store.select(fromRoot.getListShowCreate);
    let listState = store.select(fromRoot.getListState);

    this.secrets$ = Observable.combineLatest(
      store.select(fromRoot.getSecrets),
      listState.select("searchText"),
      listState.select("secretManaged"),
      filterOnSearch,
    );

    this.groups$ = store.select(fromRoot.getGroups);

    listState.select("secretManaged").subscribe((secretManaged: number) => {
      this.secretManaged = secretManaged;
    });
  }

  public ngOnInit() {
    this.getSecrets().then(() => {
      // web ext popup uses this
      this.route.params.subscribe((params) => {
        if (params["id"]) {
          let secretId = Number(params["id"]);
          this.secretIdFromUrl = secretId;
          // this.finishedUpdating = false;
          this.store.dispatch(new list.SetManagedSecret(secretId));
        }
      });
    });
    this.getGroups();
  }

  /** Trigger refresh of secrets data */
  public getSecrets() {
    return this.secretService.getSecrets();
  }

  public getGroups() {
    this.groupService.getGroups();
  }

  /*
  * receive secret from child when secret clicked
  */
  public secretWasSelected(secret: Secret) {
    this.finishedUpdating = false;
    if (this.secretManaged === secret.id) {
      this.store.dispatch(new list.ClearManagedSecret());
    } else {
      this.store.dispatch(new list.SetManagedSecret(secret.id!));
    }
  }

  public refreshSecretList(value: boolean) {
    value ? this.getSecrets() : value = true;
  }

  public searchUpdate(term: string) {
    this.store.dispatch(new list.SetSearchText(term));
  }

  /** Create a new secret and hide the new secret form */
  secretAddSave(event: {secret: ISecret, groups: number[]}) {
    let secret = event.secret;
    let groups = event.groups;
    if (!secret.secrets) {  // This can happen because the form won't include it when blank/unedited
      secret.secrets = [];
    }
    this.secretService.createSecrets(secret).then((newSecret) => {
      this.secretService.updateGroupsForSecret(groups, newSecret!.id!).then(() => {
        this.refreshSecretList(true);
        this.store.dispatch(new list.HideCreate());
      });
    }).catch((err) => {
      console.error(err);
    });
  }

  // Save and existing secret and it's groups
  saveSecret(event: {secret: ISecret, groups: number[]}) {
    let newSecret = event.secret;
    let groups = event.groups;
    this.secretService.updateGroupsForSecret(groups, newSecret.id).then(() => {
      this.secretService.updateSecret(newSecret).then((secret) => {
        this.store.dispatch(new secretActions.ReplaceSecretAction(secret));
        this.finishedUpdating = true;
      });
    });
  }

  deleteSecret(secret: ISecret) {
    this.secretService.deleteSecret(secret.id).then(() => {
      this.store.dispatch(new secretActions.RemoveSecretAction(secret.id));
    });
  }

  showAddSecretForm = () => {
    this.store.dispatch(new list.ClearManagedSecret());
    this.store.dispatch(new list.ShowCreate());
  }

  hideAddSecretForm = () => {
    this.store.dispatch(new list.HideCreate());
  }
}
