import { Component, EventEmitter, Input, Output } from "@angular/core";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs/Rx";

import { IDBSecret, IGroup, ISecret } from "passit-sdk-js/passit_sdk/interfaces";
import { Secret } from "../../secrets";
import { SecretService } from "../../secrets/secret.service";
import { copyToClipboard } from "../../utils";

interface ISelectOptions {
  label: string;
  value: any;
}

@Component({
  selector: "secret-form",
  styleUrls: ["../secret-row/secret-row.component.scss"],
  templateUrl: "./secret-form.component.html",
})
export class SecretFormComponent {
  secret: Secret = {
    name: undefined,
    type: undefined,
    secrets: {},
    visible_data: {},
  };
  visibleData: any = {};
  plaintextSecrets = {};
  passwordIsMasked = true;
  groupOptions: ISelectOptions[] = [];
  secretIsUpdating: boolean = false;
  secretIsUpdated: boolean = false;
  selectedGroups: number[] = [];
  showNotes = false;
  displayType = "text";
  SECRET_FIELDS = ["password", "notes"];
  isNew = true;

  @Output() hideAddSecretForm = new EventEmitter();
  @Output() saveSecret = new EventEmitter<{secret: ISecret, groups: number[]}>();
  @Output() deleteSecret = new EventEmitter<ISecret>();

  constructor(private secretService: SecretService) {}

  @Input()
  set groups(groups: IGroup[]) {
    this.groupOptions = groups.map((group) => {
      return {
        label: group.name,
        value: group.id,
      };
    });
  }

  @Input()
  set dbSecret(dbSecret: IDBSecret) {
    this.isNew = false;
    this.secret = new Secret(dbSecret);
    this.secretService.showSecret(this.secret.dbSecret!).then((secrets) => {
      this.plaintextSecrets = this.secretService.setPlaintextSecrets(secrets);
      this.visibleData = {
        url: this.secret.visible_data!.url,
        username: this.secret.visible_data!.username,
      };
      this.showNotes = this.plaintextSecrets["notes"] ? true : false;
    });
    for (let through of this.secret.dbSecret!.secret_through_set) {
      if (through.group) {
        this.selectedGroups.push(through.group);
      }
    }
  }

  @Input()
  set finishedUpdating(updated: boolean) {
    if (updated) {
      this.secretIsUpdating = false;
      this.secretIsUpdated = true;
      setTimeout(() => this.secretIsUpdated = false, 2000);
    }
  }

  onSubmit(form: any) {
    this.secretIsUpdating = true;
    // Defensive programming - ensure we only send data we want updated
    // Uses both model and form TODO use only the form or find a way to make
    // it work with just the model (which can't handle nesting).
    let newSecret: ISecret = {
      id: this.secret.id!,
      name: this.secret!.name,
      type: this.secret!.type,
      visible_data: {
        url: this.visibleData["url"],
        username: this.visibleData["username"],
      },
      secrets: this.secretService.getPlaintextSecrets(this.SECRET_FIELDS, this.plaintextSecrets),
    };

    // console.log(this.secret);
    // console.log(newSecret);

    this.saveSecret.emit({secret: newSecret, groups: this.selectedGroups});
  }

  clickMaskedSecret(el: HTMLElement) {
    this.toggleViewSecret().then(() => {
      // Eh, need to run focus right after render.
      setTimeout(() => el.focus(), 0);
    });
  }

  toggleViewSecret() {
    this.passwordIsMasked = !this.passwordIsMasked;
    return Promise.resolve();
  }

  toggleShowNotes() {
    this.showNotes = !this.showNotes;
  }

  onDeleteSecret() {
    if (window.confirm("Once it's deleted, it's gone forever. Is that okay?")) {
      this.deleteSecret.emit(this.secret as any);
    }
  }

  copySecret() {
    // TODO doesn't work in firefox
    copyToClipboard(this.plaintextSecrets["password"]);
  }

  goToUrl() {
    let url = this.secret.visible_data!["url"];
    if (!url.match(/^https?:\/\//)) {
      url = "http://" + url;
    }
    window.open(url, "_blank");
  }
}
