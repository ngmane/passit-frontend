import { DebugElement } from "@angular/core";
import { async, ComponentFixture, fakeAsync, TestBed, tick } from "@angular/core/testing";
import { FormsModule } from "@angular/forms";
import { By } from "@angular/platform-browser";
import { Router } from "@angular/router";
import { StoreModule } from "@ngrx/store";
import { InlineSVGModule } from "ng-inline-svg";
import { SelectModule } from "ng-select";
import { ClipboardModule } from "ngx-clipboard";
import { INewSecret, ISecret } from "passit-sdk-js/passit_sdk/interfaces";

import { reducers } from "../../app.reducers";
import { Api } from "../../ngsdk/api";
import { NgPassitSDK } from "../../ngsdk/sdk";
import { dbSecretExample, Secret } from "../../secrets";
import { SecretService } from "../../secrets/secret.service";
import { newEvent } from "../../testing";
import { SecretFormComponent } from "./secret-form.component";

let fixture: ComponentFixture<SecretFormComponent>;
let component: SecretFormComponent;
let page: Page;
let testSecret: Secret = {
  name: "Test Secret",
  type: undefined,
  secrets: {
    password: "hunter2",
    notes: "This is a secure note",
  },
  visible_data: {
    username: "Testy McTest",
  },
};

describe("SecretFormComponent", () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecretFormComponent ],
      imports: [
        ClipboardModule,
        FormsModule,
        InlineSVGModule,
        SelectModule,
        StoreModule.forRoot(reducers),
      ],
      providers: [
        { provide: Api, useClass: Api },
        { provide: NgPassitSDK, useClass: NgPassitSDK},
        { provide: Router },
        { provide: SecretService, useClass: SecretService},
      ],
    })
    .compileComponents();
  }));

  describe("Create new secrets", newSecretSetup);
  // describe("Modify existing Secrets", existingSecretSetup);
});

function newSecretSetup() {
  beforeEach(async(() => {
    createComponent();
  }));

  it("creates secret form", () => {
    let secretComponent: any = fixture.debugElement.query(By.css(".secret")).nativeElement;
    expect(secretComponent.textContent).toContain("Username");
  });

  it("translates text input into proper data", () => {
    page.nameField.nativeElement.value = testSecret.name;
    page.usernameField.nativeElement.value = testSecret.visible_data!.username;
    page.passwordField.nativeElement.value = testSecret.secrets!.password;
    page.notesField.nativeElement.value = testSecret.secrets!.notes;

    page.nameField.nativeElement.dispatchEvent(newEvent("input"));
    page.usernameField.nativeElement.dispatchEvent(newEvent("input"));
    page.passwordField.nativeElement.dispatchEvent(newEvent("input"));
    page.notesField.nativeElement.dispatchEvent(newEvent("input"));

    expect(component.secret.name).toEqual(testSecret.name);
    expect(component.visibleData["username"]).toEqual(testSecret.visible_data!.username);
    expect(component.plaintextSecrets["password"]).toEqual(testSecret.secrets!.password);
    expect(component.plaintextSecrets["notes"]).toEqual(testSecret.secrets!.notes);

    page.submitForm();
    const submittedSecret = page.getSubmittedData();

    const expectedSecret: ISecret = {
      name: testSecret.name,
      type: undefined,
      id: undefined!,
      visible_data: {
        username: testSecret.visible_data!.username,
        url: undefined,
      },
      secrets: {
        password: testSecret.secrets!.password,
        notes: testSecret.secrets!.notes,
      },
    };

    expect(page.getSubmitSpyCalls()).toBe(true);
    expect(submittedSecret).toEqual(expectedSecret);
  });

  it("creates an object with one secure property", () => {
    component.secret.name = testSecret.name;
    component.plaintextSecrets["password"] = testSecret.secrets!.password;

    page.submitForm();
    const submittedSecret = page.getSubmittedData();

    const expectedSecret: ISecret = {
      name: testSecret.name,
      type: undefined,
      id: undefined!,
      visible_data: {
        username: undefined,
        url: undefined,
      },
      secrets: {
        password: testSecret.secrets!.password,
      },
    };

    expect(submittedSecret).toEqual(expectedSecret);
  });

  it("creates an object with no secure properties", () => {
    component.secret.name = testSecret.name;

    page.submitForm();
    const submittedSecret = page.getSubmittedData();

    const expectedSecret: ISecret = {
      name: testSecret.name,
      type: undefined,
      id: undefined!,
      visible_data: {
        username: undefined,
        url: undefined,
      },
      secrets: {},
    };

    expect(submittedSecret).toEqual(expectedSecret);
  });
}

// function existingSecretSetup() {
//   beforeEach(async(() => {
//     createComponent(dbSecretExample);
//   }));

//   it("creates secret form", () => {
//     let secretComponent: any = fixture.debugElement.query(By.css(".secret")).nativeElement;
//     expect(secretComponent.textContent).toContain("Username");
//   });
// }

// Helpers, in the style of https://angular.io/docs/ts/latest/testing/#!#page-object
function createComponent(dbSecret?: any) {
  fixture = TestBed.createComponent(SecretFormComponent);
  component = fixture.componentInstance;
  component.showNotes = true;
  if (dbSecret) {
    component.dbSecret = dbSecret;
  }
  page = new Page();

  // 1st change detection triggers ngOnInit which gets a hero
  fixture.detectChanges();
  return fixture.whenStable().then(() => {
    // 2nd change detection displays the async-fetched hero
    fixture.detectChanges();
    page.addPageElements();
  });
}

class Page {
  submitSpy: jasmine.Spy;

  form: DebugElement;
  nameField: DebugElement;
  usernameField: DebugElement;
  passwordField: DebugElement;
  notesField: DebugElement;

  constructor() {
    this.submitSpy = spyOn(component.saveSecret, "emit");
  }

  /** Add page elements after hero arrives */
  addPageElements() {
    this.form = fixture.debugElement.query(By.css("form"));
    this.nameField = fixture.debugElement.query(By.css("#nameInput"));
    this.usernameField = fixture.debugElement.query(By.css("#usernameInput"));
    this.passwordField = fixture.debugElement.query(By.css("#passwordInput"));
    if (component.secret.id) {
      // blarg
    }
    if (component.showNotes) {
      this.notesField = fixture.debugElement.query(By.css("#notes"));
    }
  }

  submitForm() {
    this.form.triggerEventHandler("submit", null);
  }

  getSubmittedData() {
    // This feels dirty
    return this.submitSpy.calls.mostRecent().args[0].secret;
  }

  getSubmitSpyCalls() {
    return this.submitSpy.calls.any();
  }
}
