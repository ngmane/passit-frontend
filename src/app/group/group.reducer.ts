import { ActionReducer } from "@ngrx/store";
import * as group from "./group.actions";

import { IGroup } from "./group.interfaces";
import { IGroupForm } from "./group.interfaces";

export interface IGroupState {
  groupForm: IGroupForm;
  groupManaged?: number;
  groups: IGroup[];
  showCreate: boolean;
}

const initialState: IGroupState = {
  groupForm: {
    members: [],
    name: "",
  },
  groups: [],
  showCreate: false,
};

export function groupReducer(state = initialState, action: group.Actions): IGroupState {
  switch (action.type) {
    case group.ADD_GROUP: {
      return Object.assign({}, state, {
        groups: [
          ...state.groups,
          (action as group.AddGroup).payload,
        ],
      });
    }

    case group.REMOVE_GROUP: {
      let groups = state.groups.filter((group) => {
        return (group.id !== (action as group.RemoveGroup).payload);
      });
      return Object.assign({}, state, {
        groups
      });
    }

    case group.RESET_BLANK_GROUP_FORM: {
      return Object.assign({}, state, {
        groupForm: initialState.groupForm,
      });
    }

    case group.SET_GROUPS:
      return Object.assign({}, state, {
        groups: (action as group.SetGroupsAction).payload,
      });

    case group.SHOW_GROUPS_CREATE:
      return Object.assign({}, state, {
        showCreate: true,
      });

    case group.HIDE_GROUPS_CREATE:
      return Object.assign({}, state, {
        showCreate: false,
      });

    case group.CLEAR_MANAGED_GROUP:
      return Object.assign({}, state, {
        groupManaged: null,
      });

    case group.SET_MANAGED_GROUP:
      return Object.assign({}, state, {
        groupManaged: (action as group.SetManagedGroupAction).payload,
      });

    case "[Router] Update Location":
      return Object.assign({}, state, {
        groupManaged: null,
        showCreate: false,
      });

    default:
      return state;
  }
}

export const getGroups = (state: IGroupState) => state.groups;
export const getGroupShowCreate = (state: IGroupState) => state.showCreate;
export const getGroupForm = (state: IGroupState) => state.groupForm;
export const getGroupManaged = (state: IGroupState) => state.groupManaged;
