import { ChangeDetectionStrategy, Component, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs/Rx";

import { IGroup } from "passit-sdk-js/passit_sdk/interfaces";
import * as fromRoot from "../app.reducers";
import { IContact } from "./contacts/contacts.interfaces";
import { ContactsService } from "./contacts/contacts.service";
import * as groupActions from "./group.actions";
import { IGroupForm } from "./group.interfaces";
import { GroupService } from "./group.service";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "group",
  styleUrls: ["./group.component.scss"],
  template: `
    <group-add-button (showAddGroupForm)="showAddGroupForm()"></group-add-button>
    <group-list
      [groups]="groups$ | async"
      [groupManaged]="groupManaged$ | async"
      [contacts]="contacts$ | async"
      [showCreate]="showCreate$ | async"
      (groupSelected)="groupSelected($event)"
      (hideAddSecretForm)="hideAddSecretForm()"
      (saveNew)="groupAddSave($event)"
      (save)="groupEditSave($event)"
      (delete)="groupDelete($event)"
    ></group-list>
  `,
})
export class GroupContainer implements OnInit {
  showCreate$: Observable<boolean>;
  groups$: Observable<IGroup[]>;
  groupManaged$: Observable<number|undefined>;
  groupManaged: number;
  contacts$: Observable<IContact[]>;

  constructor(
    private groupService: GroupService,
    private ContactsService: ContactsService,
    private store: Store<fromRoot.IState>
  ) {
    this.showCreate$ = store.select(fromRoot.getGroupShowCreate);
    this.groups$ = store.select(fromRoot.getGroups);
    this.groupManaged$ = store.select(fromRoot.getGroupManaged);
    this.contacts$ = store.select(fromRoot.getContacts);

    this.groupManaged$.subscribe((groupManaged: number) => {
      this.groupManaged = groupManaged;
    });
  }

  ngOnInit() {
    this.getContacts();
    this.getGroups();
  }

  /** Refresh contacts list state */
  getContacts() {
    return this.ContactsService.getContacts();
  }

  /* update groups list state */
  getGroups() {
    return this.groupService.getGroups();
  }

  /** Create a new group and hide the new group form */
  groupAddSave(event: IGroupForm) {
    this.groupService.create(event).then(() => {
      this.store.dispatch(new groupActions.HideGroupsCreate());
      this.getGroups();
    });
  }

  /** Save a existing group and refresh list of all groups */
  groupEditSave(form: IGroupForm) {
    this.groupService.updateGroupMembers(form).then(() => {
      this.groupService.update(form).then(() => {
        this.getGroups();
      });
    });
  }

  /** Delete this group by id, then refresh groups */
  groupDelete(groupId: number) {
    this.groupService.deleteGroup(groupId).then(() => {
      this.getGroups();
    });
  }

  /** Show the create new group form */
  showAddGroupForm() {
    this.store.dispatch(new groupActions.ClearManagedGroupAction());
    this.store.dispatch(new groupActions.ShowGroupsCreate());
  }

  /** Show the detail/edit view of a group selected */
  groupSelected(group: IGroup) {
    if (this.groupManaged === group.id) {
      this.store.dispatch(new groupActions.ClearManagedGroupAction());
    } else {
      this.store.dispatch(new groupActions.HideGroupsCreate());
      this.store.dispatch(new groupActions.SetManagedGroupAction(group.id!));
    }
  }

  hideAddSecretForm = () => {
    this.store.dispatch(new groupActions.HideGroupsCreate());
  }
}
