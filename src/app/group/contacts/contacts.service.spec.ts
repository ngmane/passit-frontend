import { fakeAsync, inject, TestBed } from "@angular/core/testing";
import { HttpModule, Response, ResponseOptions, XHRBackend} from "@angular/http";
import { MockBackend, MockConnection} from "@angular/http/testing";
import { Store, StoreModule } from "@ngrx/store";

import { getConfState, IState, reducers } from "../../app.reducers";
import { Api } from "../../ngsdk/api";
import { NgPassitSDK } from "../../ngsdk/sdk";
import { ContactsService } from "./contacts.service";

describe("Contacts Service", () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpModule,
        StoreModule.forRoot(reducers),
      ],
      providers: [
        Api,
        NgPassitSDK,
        ContactsService,
        {provide: XHRBackend, useClass: MockBackend},
      ]
    });
  });

  it("look up an id by email",
    inject([XHRBackend, ContactsService],
    fakeAsync((mockBackend: MockBackend, service: ContactsService) => {
    mockBackend.connections.subscribe(
      (connection: MockConnection) => {
        connection.mockRespond(new Response(
          new ResponseOptions({body: {id: 1}}
        )));
      }
    );
    service.lookupUserByEmail("test@example.com").then((id) => {
      expect(id).toBe(1);
    });
  })));
});
