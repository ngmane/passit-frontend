import { Action } from "@ngrx/store";

import { IContact } from "./contacts.interfaces";

export const SET_CONTACTS = "Set Contacts";

export class SetContacts implements Action {
  type = SET_CONTACTS;

  constructor(public payload: IContact[]) { }
}

export type Actions
  = SetContacts;
