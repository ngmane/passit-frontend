import { ActionReducer } from "@ngrx/store";
import * as contacts from "./contacts.actions";
import { IContact } from "./contacts.interfaces";

export interface IContactsState {
  contacts: IContact[];
}

const initialState: IContactsState = {
  contacts: [],
};

export function contactsReducer(state = initialState, action: contacts.Actions): IContactsState {
  switch (action.type) {
    case contacts.SET_CONTACTS:
      return Object.assign({}, state, {
        contacts: action.payload,
      });

  default:
    return state;
  }
}

export const getContacts = (state: IContactsState) => state.contacts;
