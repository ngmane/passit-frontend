import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";

import * as fromRoot from "../../app.reducers";
import { Api } from "../../ngsdk/api";
import { NgPassitSDK } from "../../ngsdk/sdk";
import { SetContacts } from "./contacts.actions";
import { IContact } from "./contacts.interfaces";

interface ILookupUserIdResponse {
  id: number;
}

@Injectable()
export class ContactsService {
  constructor(private api: Api, public sdk: NgPassitSDK, private store: Store<fromRoot.IState>) {}

  /** Get all contacts and save them in state */
  public getContacts() {
    return this.sdk.list_contacts().then((contacts) => {
      this.store.dispatch(new SetContacts(contacts));
      return contacts;
    });
  }

  /** Look up a user id by exact email address
   * Returns promise with user ID or rejection if no user with this email exists
   */
  public lookupUserByEmail(email: string): Promise<number> {
    return new Promise((resolve, reject) => {
      const url = "lookup-user-id/";
      let data = {
        email,
      };
      return this.api.jsonPost(url, data)
        .then((resp: ILookupUserIdResponse) => {
          resolve(resp.id);
        })
        .catch(() => {
          reject();
        });
    });
  }
}
