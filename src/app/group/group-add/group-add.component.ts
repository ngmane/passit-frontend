import { Component, EventEmitter, Input, Output } from "@angular/core";

import { IContact } from "../contacts/contacts.interfaces";
import { IGroup, IGroupForm } from "../group.interfaces";
import { GroupService } from "../group.service";

@Component({
  selector: "group-add",
  styleUrls: ["../../list/secret-row/secret-row.component.scss", "../../list/list.component.scss"],
  template: `
    <div class="secret secret--add">
      <h2 class="secret__title">
        Add New Group
      </h2>

      <group-form
        isNew="true"
        [contacts]="contacts"
        (hideAddSecretForm)="hideAddSecretForm.emit()"
        (save)="onSave($event)"
        class="secret-form secret-form--is-active"
      ></group-form>
    </div>
  `,
})
export class GroupAddComponent {
  group: IGroup;
  @Input() contacts: IContact[];
  @Output() hideAddSecretForm = new EventEmitter();
  @Output() saveNew = new EventEmitter();
  public onGroupCreated: EventEmitter<boolean>;

  public onSave(event: Event) {
    this.saveNew.emit(event);
  }
}
