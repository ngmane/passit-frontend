import { Component, EventEmitter, Input, Output } from "@angular/core";

import { IGroup } from "passit-sdk-js/passit_sdk/interfaces";
import { IContact } from "./contacts/contacts.interfaces";
import { IGroupForm } from "./group.interfaces";

@Component({
  selector: "group-list",
  styleUrls: ["./group.component.scss", "../list/list.component.scss"],
  templateUrl: "./group-list.component.html",
})
export class GroupListComponent {
  groupAnimated = false;

  @Input() groups: IGroup[];
  @Input() groupManaged: number;
  @Input() contacts: IContact[];
  @Input() showCreate: boolean;

  @Output() groupSelected = new EventEmitter<IGroup>();
  @Output() hideAddSecretForm = new EventEmitter();
  @Output() save = new EventEmitter<IGroupForm>();
  @Output() saveNew = new EventEmitter<IGroupForm>();
  @Output() delete = new EventEmitter<number>();

  onGroupWasSelected(group: IGroup) {
    this.groupAnimated = true;
    // I don't know why this doesn't work but similar code in list.component.ts does
    // if (this.groupManaged !== group.id) {
    //   setTimeout(() => {
    //     this.groupAnimated = true;
    //   }, 1);
    // }
    this.groupSelected.emit(group);
  }
}
