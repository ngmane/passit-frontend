/* tslint:disable:max-classes-per-file */
// tslint:disable:object-literal-sort-keys
import { Action } from "@ngrx/store";

import { IGroup } from "passit-sdk-js/passit_sdk/interfaces";

export const ADD_GROUP = "Add Group";
export const HIDE_GROUPS_CREATE = "Hide Groups Create";
export const REMOVE_GROUP = "Remove Group";
export const RESET_BLANK_GROUP_FORM = "Reset Blank Group Form";
export const SET_GROUPS = "Set Groups";
export const SHOW_GROUPS_CREATE = "Show Groups Create";
export const CLEAR_MANAGED_GROUP = "clear managed group";
export const SET_MANAGED_GROUP = "Set managed group";

export class AddGroup implements Action {
  public type = ADD_GROUP;

  constructor(public payload: IGroup) { }
}

export class HideGroupsCreate implements Action {
  public type = HIDE_GROUPS_CREATE;
}

export class RemoveGroup implements Action {
  public type = REMOVE_GROUP;

  constructor(public payload: number) { }
}

export class ResetBlankGroupForm implements Action {
  public type = RESET_BLANK_GROUP_FORM;
}

export class SetGroupsAction implements Action {
  public type = SET_GROUPS;

  constructor(public payload: IGroup[]) { }
}

export class ShowGroupsCreate implements Action {
  public type = SHOW_GROUPS_CREATE;
}

export class ClearManagedGroupAction implements Action {
  public type = CLEAR_MANAGED_GROUP;
}

export class SetManagedGroupAction implements Action {
  public type = SET_MANAGED_GROUP;

  constructor(public payload: number) { }
}

export type Actions
  = AddGroup
  | HideGroupsCreate
  | RemoveGroup
  | ResetBlankGroupForm
  | SetGroupsAction
  | ShowGroupsCreate
  | ClearManagedGroupAction
  | SetManagedGroupAction;
