import {Component, EventEmitter, Input, Output} from "@angular/core";
import { IContact } from "../contacts/contacts.interfaces";
import { IGroup, IGroupForm } from "../group.interfaces";

interface ISelectOptions {
  label: string;
  value: any;
}

@Component({
    selector: "group-form",
    styleUrls: ["../../list/secret-row/secret-row.component.scss"],
    templateUrl: "./group-form.component.html",
})
export class GroupFormComponent {
  // tslint:disable-next-line:variable-name
  _group?: IGroup;
  groupForm: IGroupForm = {
    members: [],
    name: "",
  };
  memberOptions: ISelectOptions[] = [];

  @Output() back = new EventEmitter();
  @Output() hideAddSecretForm = new EventEmitter();
  @Output() save = new EventEmitter<IGroupForm>();
  @Output() delete = new EventEmitter<number>();

  @Input() isNew = false;
  @Input() set group(group: IGroup) {
    this._group = group;
    let selectedMembers = [];
    for (let groupuser of this._group.groupuser_set) {
      selectedMembers.push(groupuser.user);
    }
    this.groupForm = Object.assign({}, {
      id: group.id,
      members: selectedMembers,
      name: group.name,
      slug: group.slug,
    });
  }
  get group() {
    return this._group!;
  }

  @Input()
  set contacts(contacts: IContact[]) {
    this.memberOptions = contacts.map((contact) => {
      return {
        label: contact.email,
        value: contact.id,
      };
    });
  }

  onSubmit() {
    this.save.emit(this.groupForm);
  }

  onDelete() {
    if (window.confirm("Once it's deleted, it's gone forever. Is that okay?")) {
      this.delete.emit(this.group.id);
    }
  }
}
