import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Store } from "@ngrx/store";

import * as fromRoot from "../app.reducers";
import { NgPassitSDK } from "../ngsdk/sdk";
import { LOG_OUT } from "../shared/app-data/authState.actions";
import { AddGroup, SetGroupsAction } from "./group.actions";
import { IGroupForm } from "./group.interfaces";

@Injectable()
export class GroupService {
  constructor(public sdk: NgPassitSDK, private store: Store<fromRoot.IState>, private router: Router) {}

  /*
  * add group to secret using group id and secret id
  */
  public addGroupToSecret(groupId: number, secret: any) {
    return this.sdk.add_group_to_secret(groupId, secret.id)
      .then((resp) => resp)
      .catch((err) => console.error(err));
  }

  /*
  * create group
  */
  public create(form: IGroupForm) {
    // Add current user always!
    let userId = this.sdk.userId;
    if (form.members.indexOf(userId) === -1) {
      form.members.push(userId);
    }
    return this.sdk.create_group(form.name!)
      .then((resp) => {
        form.id = resp.id;
        return this.updateGroupMembers(form);
      })
      .catch((err) => console.error(err));
  }

  public update(form: IGroupForm) {
    return this.sdk.update_group({id: form.id!, name: form.name!, slug: form.slug!})
      .catch((err) => console.error(err));
  }

  /*
  * get specific group using id
  */
  public getGroup(groupId: number): Promise<any> {
    return this.sdk.get_group(groupId)
      .then((resp) => resp)
      .catch((err) => console.error(err));
  }

  /**
   * Get groups from sdk and place them in state store.
   * Returns Promise indicating result.
   */
  public getGroups(): Promise<any> {
    return this.sdk.list_groups()
      .then((resp) => {
        this.store.dispatch(new SetGroupsAction(resp));
        return resp;
      })
      .catch((err) => {
        this.handleError(err);
      });
  }

  public removeGroupFromSecret(secret: any) {
    return this.sdk.get_secret(secret.id)
      .then((data) => {
        return this.sdk.remove_group_from_secret(secret.id, data.secret_through_set[0].id!)
          .then((resp) => {
            return resp;
          })
          .catch((err) => console.error(err));
      })
      .catch((err) => console.error(err));
  }

  public updateGroupMembers(groupForm: IGroupForm) {
    let members = groupForm.members;
    return new Promise((resolve, reject) => {
      this.sdk.get_group(groupForm.id!).then((group) => {
        let existingMembers = [];
        for (let usergroup of group.groupuser_set) {
          existingMembers.push(usergroup.user);
        }
        let promises = [];
        for (let member of members) {
          // If new member is not an existing member then add them
          if (existingMembers.indexOf(member) < 0) {
            promises.push(this.sdk.add_user_to_group(group.id!, member));
          }
        }
        for (let member of existingMembers) {
          // If existing member is not in new group members then remove them
          if (members.indexOf(member) < 0) {
            let memberGroupuser = group.groupuser_set.find((groupuser) => groupuser.user === member);
            promises.push(this.sdk.remove_user_from_group(group.id!, memberGroupuser!.id));
          }
        }
        Promise.all(promises).then(() => resolve());
      });
    });
  }

  public deleteGroup(groupId: number) {
    return this.sdk.delete_group(groupId);
  }

  private handleError(err: any) {
    console.error(err);
    if (err.res) {
      if (err.res.status === 401) {
        this.store.dispatch({ type: LOG_OUT });
      }
    }
    if (err.res.json().detail === "User's email is not confirmed.") {
      this.router.navigate(["/confirm-email"]);
    }
  }
}
