export interface IGroupForm {
  id?: number;
  name?: string;
  slug?: string;
  members: number[];
}

export { IGroup } from "passit-sdk-js/passit_sdk/interfaces";
