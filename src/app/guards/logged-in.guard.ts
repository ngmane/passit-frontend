import { Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from "@angular/router";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs";

import * as fromRoot from "../app.reducers";
import { getAuthState } from "../app.reducers";
import { SET_REDIRECT_URL } from "../shared/app-data/authState.actions";

@Injectable()
export class LoggedInGuard implements CanActivate {
  constructor(private store: Store<fromRoot.IState>, private router: Router) { }

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let url: string = state.url;

    return this.checkLogin(url);
  }

  private checkLogin(url: string) {
      let isLoggedIn = this.store.pluck("authState", "isLoggedIn").first() as Observable<boolean>;
      isLoggedIn.filter((t, _) => !t).subscribe((_) => {
        this.store.dispatch({
          type: SET_REDIRECT_URL,
          payload: url,
        });
        this.router.navigate(["/login/"]);
      });
      return isLoggedIn;

      // if (this.authService.isLoggedIn) { return true; }

      // // Navigate to the login page with extras
      // this.router.navigate(["/login"]);
      // return false;
    }

}
