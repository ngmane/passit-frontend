import { Action, ActionReducer } from "@ngrx/store";
import { Actions, SET_CONF } from "./conf.actions";

export interface IConfState {
  isDemo: boolean;
  timestamp: string | null;
}

export const initial: IConfState = {
  isDemo: false,
  timestamp: null,
};

export function reducer(state = initial, action: Actions): IConfState {
  switch (action.type) {
    case SET_CONF: {
      return {
        isDemo: action.payload.IS_DEMO,
        timestamp: new Date().toString(),
      };
    }

    default: {
      return state;
    }
 }
}

export const getIsDemo = (state: IConfState) => state.isDemo;
export const getTimestamp = (state: IConfState) => state.timestamp;
