import { Action } from "@ngrx/store";

import { IConfResponse } from "./conf.interfaces";

export const SET_CONF = "[conf] Set Conf";

/** Set entire conf state from server */
export class SetConfAction implements Action {
  readonly type = SET_CONF;

  constructor(public payload: IConfResponse) { }
}

export type Actions
  = SetConfAction;
