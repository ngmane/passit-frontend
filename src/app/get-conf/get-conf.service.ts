import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";
import "rxjs/add/operator/take";

import * as fromRoot from "../app.reducers";
import { Api } from "../ngsdk/api";

import { SetConfAction } from "./conf.actions";
import { IConfResponse } from "./conf.interfaces";

@Injectable()
export class GetConfService {
  constructor(private api: Api, private store: Store<fromRoot.IState>) {}

  /** Init all configuration data, potentially fetching from server if needed
   */
  public rehydrate() {
    return new Promise((resolve, reject) => {
      this.store.select(fromRoot.getTimestamp).take(1).subscribe((timestamp) => {
        if (timestamp === null) {
          resolve(this.getConf());
        } else {
          resolve();
        }
      });
    });
  }

  /**
   * Get configuration from server and save it to local state
   */
  private getConf() {
    const url = "conf/";
    return this.api.jsonGet(url).then((resp: IConfResponse) => {
      this.store.dispatch(new SetConfAction(resp));
    });
  }
}
