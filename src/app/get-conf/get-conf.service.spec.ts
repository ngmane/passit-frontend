import { fakeAsync, inject, TestBed } from "@angular/core/testing";
import { Store, StoreModule } from "@ngrx/store";

import { Api } from "../ngsdk/api";

import { getConfState, IState, reducers } from "../app.reducers";
import { IConfResponse } from "./conf.interfaces";
import { GetConfService } from "./get-conf.service";

class FakeAPI {
  public jsonGet(url: string): Promise<{}> {
    return new Promise((resolve, reject) => {
      resolve({
        IS_DEMO: true,
      });
    });
  }
}

describe("Get Conf Service", () => {
   beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot(reducers),
      ],
      providers: [
        Api,
        GetConfService,
        {provide: Api, useClass: FakeAPI},
      ]
    });
  });

   it("get conf from server and save it to local state",
    inject([GetConfService, Store],
    fakeAsync((service: GetConfService, store: Store<IState>
  ) => {
    service.rehydrate().then(() => {
      store.select(getConfState).subscribe((confState) => {
        expect(confState.isDemo).toBe(true);
        expect(confState.timestamp).toBeTruthy();
      });
    });
  })));
});
