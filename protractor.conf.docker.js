const { config } = require('./protractor.conf');

config.baseUrl = 'http://app:4200/'
config.directConnect = false
config.seleniumAddress = 'http://selenium:4444/wd/hub/'

exports.config = config


