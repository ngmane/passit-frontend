import { browser, by, element, Key } from "protractor";

export class GroupsPage {
  navigateTo = () => browser.get("/groups/");
  getNewGroupBtnElem = () => element(by.id("showAddGroupform"));

  // new form
  cancelFormByClick = () => element(by.className("group-form__cancel-btn")).click();
  enterGroupName = (name: string) => element(by.id("nameInput")).sendKeys(name);
  newGroup = () => element(by.id("showAddGroupform")).click();

  // edit form
  deleteGroupByClick = () => element(by.className("group-form__delete-btn")).click();
  editGroupName = (name: string) => this.enterGroupName(name);
  getManageGroupToggleBtnElem = () => element(by.id("manageSecretBtn"));

  // element selectors and actions
  getAllGroupsElem = () => element.all(by.css(".secret-list-item"));
  getActiveGroupFormCompElem = () => element(by.tagName("group-form"));
  getFormElem = () => element(by.className("secret__details"));
  getGroupTitleElem = () => element(by.css(".secret__heading .secret__title"));
  getNameFormInputElem = () => element(by.id("nameInput"));
  getNewlyCreatedGroupElem = () => element.all(by.css(".secret-list-item")).last();
  saveGroupByClick = () => element(by.className("group-form__save-btn")).click();
  submitGroupByEnter = () => element(by.id("nameInput")).sendKeys(Key.ENTER);

}
