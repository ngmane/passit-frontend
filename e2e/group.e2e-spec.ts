import { ExpectedConditions as EC } from "protractor";
import { browser, ElementFinder } from "protractor";

import { AppPage, ListPage } from "./app.po";
import { GroupsPage } from "./group.po";
import { checkAndLogin } from "./shared";

describe("List, Add, Edit and Remove Groups", () => {
  let groupsPage: GroupsPage;
  let listPage: ListPage;
  let app: AppPage;

  let mockGroupFormData = [
    { name: "Test" },
    { name: "Second Test" }
  ];
  let mockEditGroupFormData = [
    { name: "Test Edited" },
    { name: "Second Test Edited" }
  ];

  beforeAll(() => {
    checkAndLogin("/groups/");
    groupsPage = new GroupsPage();
    groupsPage.navigateTo();
    browser.wait(EC.presenceOf(groupsPage.getNewGroupBtnElem()), 10000, "form is not displyed");
  });

  beforeEach(() => {
    groupsPage = new GroupsPage();
    listPage = new ListPage();
    app = new AppPage();
  });

  it("in groups page and logged in", () => {
    browser
      .getCurrentUrl()
      .then((currentUrl) => {
        expect<any>(currentUrl).toEqual(browser.baseUrl + "groups");
      });
  });

  it("Add groups", () => {
    let formEle = groupsPage.getFormElem();
    let newGroupItem: ElementFinder;
    let groupTitleElem: ElementFinder;

    let displayFrom = () => {
      groupsPage.newGroup();
      browser.wait(EC.presenceOf(formEle), 10000, "form is not displyed");
    };

    let createGroup = (name: string) => {
      groupsPage.enterGroupName(name);
      groupsPage.submitGroupByEnter();
      browser.wait(EC.stalenessOf(formEle), 10000, "group not saved and form is still open");
    };

    let getGroupTitleText = () => {
      newGroupItem = groupsPage.getNewlyCreatedGroupElem();
      groupTitleElem = groupsPage.getGroupTitleElem();
      return newGroupItem.element(groupTitleElem.locator()).getText();
    };

    mockGroupFormData.forEach((newGroupData, index) => {
      displayFrom();
      createGroup(newGroupData.name);
      expect<any>(getGroupTitleText()).toBe(newGroupData.name);
    });

  });

  it("check if the newly added groups are added correctly", () => {

    groupsPage.getAllGroupsElem().each((groupElem, index) => {
      let currentGroupTitleElem = groupElem!.element(groupsPage.getGroupTitleElem().locator());
      expect<any>(currentGroupTitleElem.getText()).toBe(mockGroupFormData[index!].name);
    });

  });

  it("open, close and check group edit form: to confirm form has correct details and display toggle works", () => {

    groupsPage.getAllGroupsElem().each((groupElem, index) => {
      let currentGroupToggleBtnElem = groupElem!.element(groupsPage.getManageGroupToggleBtnElem().locator());

      // Check no form item is displayed initially
      expect<any>(groupsPage.getFormElem().isPresent()).toBeFalsy();
      expect<any>(currentGroupToggleBtnElem.getText()).toBe("Manage");

      // Open form and check form details
      currentGroupToggleBtnElem.click();
      browser.wait(EC.presenceOf(groupsPage.getFormElem()), 10000, "form is not opened");
      expect<any>(groupsPage.getNameFormInputElem().getAttribute("value")).toBe(mockGroupFormData[index!].name);
      expect<any>(currentGroupToggleBtnElem.getText()).toBe("Close");

      // close form and confirm if its done properly
      currentGroupToggleBtnElem.click();
      browser.wait(EC.stalenessOf(groupsPage.getFormElem()), 10000, "form is not closed");
      expect<any>(currentGroupToggleBtnElem.getText()).toBe("Manage");
    });

  });

  it("group in edit mode opened should be open if url changed to list page and returned back to groups page", () => {
    let firstGroupItem: ElementFinder;
    let currentGroupToggleBtnElem: ElementFinder;

    firstGroupItem = groupsPage.getAllGroupsElem().first();
    currentGroupToggleBtnElem = firstGroupItem.element(groupsPage.getManageGroupToggleBtnElem().locator());
    currentGroupToggleBtnElem.click();
    browser.wait(EC.presenceOf(groupsPage.getFormElem()), 10000, "form is not opened");

    app.clickPasswordsNavLinkElem();
    EC.browser.wait(EC.presenceOf(listPage.getSearchElem()), 20000, "Search secrets should be visible");
    app.clickGroupsNavLinkElem();
    browser.wait(EC.presenceOf(groupsPage.getFormElem()), 10000, "form is not opened after coming back to groups page");
    expect<any>(groupsPage.getActiveGroupFormCompElem().getAttribute("class")).toContain("secret-form--is-active");
    currentGroupToggleBtnElem.click();
    browser.wait(EC.stalenessOf(groupsPage.getFormElem()), 10000, "form is not closed");
  });

  it("edit all groups", () => {

    // Note:
    // 1.after saving the current edited group it is pushed to bottom i.e its the last child now
    // 2.getting the lastest group item is required to close the form and continue
    // 3.And the next group item to be edited will always be the one with index === 1 i.e get first child
    groupsPage
      .getAllGroupsElem()
      .count()
      .then((currentGroupCount) => {
        for (let groupIndex = 0; groupIndex < currentGroupCount; groupIndex++) {
          let lastGroupItem: ElementFinder;
          let firstGroupItem: ElementFinder;
          let currentGroupToggleBtnElem: ElementFinder;

          firstGroupItem = groupsPage.getAllGroupsElem().first();
          currentGroupToggleBtnElem = firstGroupItem.element(groupsPage.getManageGroupToggleBtnElem().locator());
          currentGroupToggleBtnElem.click();
          browser.wait(EC.presenceOf(groupsPage.getFormElem()), 10000, "form is not opened");
          expect<any>(groupsPage.getNameFormInputElem().getAttribute("value"))
            .toBe(mockGroupFormData[groupIndex].name);

          groupsPage.getNameFormInputElem().clear();
          groupsPage.editGroupName(mockEditGroupFormData[groupIndex].name);
          expect<any>(groupsPage.getNameFormInputElem().getAttribute("value"))
            .toBe(mockEditGroupFormData[groupIndex].name);
          groupsPage.saveGroupByClick();
          browser.wait(EC.presenceOf(groupsPage.getFormElem()), 10000, "form is not displayed after save");

          // See Note 3
          lastGroupItem = groupsPage.getAllGroupsElem().last();
          currentGroupToggleBtnElem = lastGroupItem.element(groupsPage.getManageGroupToggleBtnElem().locator());
          currentGroupToggleBtnElem.click();
          browser.wait(EC.stalenessOf(groupsPage.getFormElem()), 10000, "form is still open");
        }
      });

  });

  it("check if edited groups are added correctly", () => {

    groupsPage.getAllGroupsElem().each((groupElem, index) => {
      let currentGroupTitleElem = groupElem!.element(groupsPage.getGroupTitleElem().locator());
      expect<any>(currentGroupTitleElem.getText()).toBe(mockEditGroupFormData[index!].name);
    });

  });

  it("remove groups: first dismiss alerts then click delete button again and accept alert", () => {
    groupsPage
      .getAllGroupsElem()
      .count()
      .then((currentGroupCount) => {
        for (let groupIndex = 0; groupIndex < currentGroupCount; groupIndex++) {
          let currentGroupItem = groupsPage.getAllGroupsElem().first();
          let currentGroupToggleBtnElem = currentGroupItem.element(groupsPage.getManageGroupToggleBtnElem().locator());
          let currentGroupTitleElem = currentGroupItem.element(groupsPage.getGroupTitleElem().locator());

          expect<any>(currentGroupTitleElem.getText()).toBe(mockEditGroupFormData[groupIndex].name);
          currentGroupToggleBtnElem.click();
          browser.wait(EC.presenceOf(groupsPage.getFormElem()), 10000, "form is not opened");

          groupsPage.deleteGroupByClick();
          browser.wait(EC.alertIsPresent(), 5000, "Alert is not displayed for group delete");
          browser.switchTo().alert().dismiss();

          groupsPage.deleteGroupByClick();
          browser.wait(EC.alertIsPresent(), 5000, "Alert is not displayed for group delete");
          browser.switchTo().alert().accept();
        }
      });

  });
});
