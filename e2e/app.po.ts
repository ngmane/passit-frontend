import { browser, by, element, Key } from "protractor";

export class AppPage {
  clickPasswordsNavLinkElem = () => element(by.partialLinkText("Passwords")).click();
  clickGroupsNavLinkElem = () => element(by.partialLinkText("Groups")).click();
}

export class LoginPage {
  navigateTo = () => browser.get("/login/");
  getParagraphText = () => element(by.css("app-root h2")).getText();
  getRegisterLink = () => element(by.id("btn-signup"));
  enterEmail = (keys: string) => element(by.id("userEmail")).sendKeys(keys);
  enterPassword = (keys: string) => element(by.id("userPassword")).sendKeys(keys);
  submitByEnter = () => element(by.id("userPassword")).sendKeys(Key.ENTER);
  getErrors = () => element(by.className("auth-form__errors"));
  getProgressTextElem = () => {
    return element(by.className("save-progress"))
      .element(element(by.className("save-progress__text")).locator());
  }
}

export class RegisterPage {
  getParagraphText = () => element(by.css("app-root h2")).getText();
  enterEmail = (keys: string) => element(by.id("userEmail")).sendKeys(keys);
  enterPassword = (keys: string) => element(by.id("userPassword")).sendKeys(keys);
  submitByEnter = () => element(by.id("userPassword")).sendKeys(Key.ENTER);
}

export class ConfirmEmailPage {
  getCodeElem = () => element(by.id("code"));
}

export class ListPage {
  getSearchElem = () => element(by.id("search"));
  navigateTo = () => browser.get("/list");
  selectAddNewPassword = () => element(by.id("add-new-password-button")).click();
  enterName = (keys: string) => element(by.id("nameInput")).sendKeys(keys);
  enterUsername = (keys: string) => element(by.id("usernameInput")).sendKeys(keys);
  enterPassword = (keys: string) => element(by.id("passwordInput")).sendKeys(keys);
  submitByEnter = () => element(by.id("passwordInput")).sendKeys(Key.ENTER);
  getSecretItem = (title: string) => {
    return element(by.xpath(`.//*[.="${title}" and class="somesecret__title"]`))
      .element(by.xpath(".."));
  }
  clickManageSecret = (title: string) => {
    return this.getSecretItem(title)
      .element(by.className("button button--gray secret__button"))
      .click();
  }
}
