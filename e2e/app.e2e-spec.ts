import { ExpectedConditions as EC } from "protractor";
import { ConfirmEmailPage, ListPage, LoginPage, RegisterPage } from "./app.po";

const USERNAME = Math.random().toString(36).slice(2) + "@example.com";
const PASSWORD = "hunter22";

describe("Login and Register", () => {
  let loginPage: LoginPage;
  let registerPage: RegisterPage;
  let confirmEmailPage: ConfirmEmailPage;
  let listPage: ListPage;

  beforeEach(() => {
    loginPage = new LoginPage();
    registerPage = new RegisterPage();
    confirmEmailPage = new ConfirmEmailPage();
    listPage = new ListPage();
  });

  it("Gives error message on incorrect login", () => {
    loginPage.navigateTo();
    loginPage.enterEmail("testerrrr@example.com");
    loginPage.enterPassword("hunter2");
    loginPage.submitByEnter();
    let elem = loginPage.getErrors();
    EC.browser.wait(EC.presenceOf(elem), 500, "Errors should have been shown");
    elem.isDisplayed();
  });

  it("Hit homepage and register", () => {
    // Users start with login page
    loginPage.navigateTo();
    expect<any>(loginPage.getParagraphText()).toEqual("Log in to Passit");

    // Go to register
    loginPage.getRegisterLink().click();
    expect<any>(registerPage.getParagraphText()).toEqual("Sign up for Passit");
    registerPage.enterEmail(USERNAME);
    registerPage.enterPassword(PASSWORD);
    registerPage.submitByEnter();
    // Should see confirm message

    let codeElem = confirmEmailPage.getCodeElem();
    EC.browser.wait(EC.presenceOf(codeElem), 25000, "Confirm code page should be shown");
    codeElem.isDisplayed();
  });

  it("Log in", () => {
    loginPage.navigateTo();
    loginPage.enterEmail(USERNAME);
    loginPage.enterPassword(PASSWORD);
    loginPage.submitByEnter();

    let searchElem = listPage.getSearchElem();
    EC.browser.wait(EC.presenceOf(searchElem), 20000, "Search secrets should be visible");
    searchElem.isDisplayed();
  });

  it("Creates a secret", () => {
    listPage.navigateTo();
    let searchElem = listPage.getSearchElem();
    EC.browser.wait(EC.presenceOf(searchElem), 1000, "Can see list");
    searchElem.isDisplayed();
    listPage.selectAddNewPassword();
    listPage.enterName("Test Secret");
    listPage.enterUsername("mctest");
    listPage.enterPassword("hunter2");
    listPage.submitByEnter();
    let secretItemElem = listPage.getSecretItem("Test Secret");
    // EC.browser.wait(EC.presenceOf(secretItemElem), 3000, "Can see new secret");
    // listPage.clickManageSecret("Test Secret");
  });
});
