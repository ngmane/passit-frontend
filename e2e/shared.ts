import { browser, by, element, Key } from "protractor";
import { ExpectedConditions as EC } from "protractor";
import { ConfirmEmailPage, LoginPage, RegisterPage  } from "./app.po";

const USERNAME = Math.random().toString(36).slice(2) + "@example.com";
const PASSWORD = "hunter22";

export const checkAndLogin  = (url: string) => {
  let loginPage = new LoginPage();
  let registerPage = new RegisterPage();
  let confirmEmailPage = new ConfirmEmailPage();

  let register = () => {
    loginPage.getRegisterLink().click();
    registerPage.enterEmail(USERNAME);
    registerPage.enterPassword(PASSWORD);
    registerPage.submitByEnter();
    let codeElem = confirmEmailPage.getCodeElem();
    EC.browser.wait(EC.presenceOf(codeElem), 25000, "Confirm code page should be shown");
    codeElem.isDisplayed();
  };

  let login = () => {
    loginPage.navigateTo();
    loginPage.enterEmail(USERNAME);
    loginPage.enterPassword(PASSWORD);
    loginPage.submitByEnter();
    browser.wait(EC.stalenessOf(element(by.css("auth-form__heading"))), 10000, "Login wait failed");
  };

  browser.get(url);

  browser.controlFlow().execute(() => {
    browser.getCurrentUrl()
      .then((currentUrl) => {
        if (currentUrl === browser.baseUrl + "login") {
          register();
          login();
        }
      });
  });
};
